package ru.nemnonovyuri.intouch.controllers;

import ru.nemnonovyuri.intouch.helpers.ImageUploads;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

@Controller
public class ImageController {

    @RequestMapping("/images/**")
    public void get(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = ImageUploads.convertUrlToPath(request.getRequestURI());
        if (path == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        File file = new File(path);
        String mimeType = request.getServletContext().getMimeType(file.getAbsolutePath());
        if (mimeType == null) {
            mimeType = "image/jpeg";
        }
        response.setContentType(mimeType);
        response.setContentLength((int) file.length());
        try (FileInputStream fis = new FileInputStream(file); OutputStream os = response.getOutputStream()) {
            byte[] buf = new byte[1024];
            int count = 0;
            while ((count = fis.read(buf)) >= 0) {
                os.write(buf, 0, count);
            }
        }
    }
}
