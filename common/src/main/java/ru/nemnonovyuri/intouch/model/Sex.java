package ru.nemnonovyuri.intouch.model;

public enum Sex {
    MALE(1),
    FEMALE(2),
    UNSPECIFIED(0);

    private int numeric;

    Sex(int numeric) {
        this.numeric = numeric;
    }

    public static Sex valueOf(int numeric) {
        for (Sex sex : Sex.values()) {
            if (sex.getNumeric() == numeric) {
                return sex;
            }
        }
        throw new RuntimeException("Invalid sex numeric presentation");
    }

    public int getNumeric() {
        return numeric;
    }
}
