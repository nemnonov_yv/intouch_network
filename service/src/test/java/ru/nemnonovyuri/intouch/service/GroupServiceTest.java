package ru.nemnonovyuri.intouch.service;

import ru.nemnonovyuri.intouch.dao.AccountDao;
import ru.nemnonovyuri.intouch.dao.GroupDao;
import ru.nemnonovyuri.intouch.dto.AccountTO;
import ru.nemnonovyuri.intouch.dto.GroupTO;
import ru.nemnonovyuri.intouch.exceptions.AccountNotFoundException;
import ru.nemnonovyuri.intouch.model.Account;
import ru.nemnonovyuri.intouch.model.Group;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class GroupServiceTest {
    @Mock
    private AccountDao accountDao;
    @Mock
    private GroupDao groupDao;
    @Spy
    @InjectMocks
    private GroupService service = new GroupService();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Group> mocks = new ArrayList<>();
        mocks.add(makeGroupMock(1, "getAll_1"));
        mocks.add(makeGroupMock(2, "getAll_2"));
        mocks.add(makeGroupMock(3, "getAll_3"));
        List<GroupTO> mockTOs = new ArrayList<>();
        for (Group g : mocks) {
            mockTOs.add(new GroupTO(g));
        }
        when(groupDao.getAll()).thenReturn(mocks);
        assertEquals(mockTOs, service.getAll());
    }

    @Test
    public void testGetGroup() throws Exception {
        int id = 1;
        when(groupDao.get(id)).thenReturn(makeGroupMock(id, "getGroup"));
        assertEquals(new GroupTO(makeGroupMock(id, "getGroup")), service.getGroup(id));
    }

    @Test
    public void testGetByTitle() throws Exception {
        List<Group> mocks = new ArrayList<>();
        mocks.add(makeGroupMock(1, "test"));
        mocks.add(makeGroupMock(2, "note"));
        List<GroupTO> mockTOs = new ArrayList<>();
        for (Group g : mocks) {
            mockTOs.add(new GroupTO(g));
        }
        when(groupDao.getByTitle("te")).thenReturn(mocks);
        assertEquals(mockTOs, service.getByTitle("te"));
    }

    @Test
    public void testCreateGroup() throws Exception {
        int uid = 7;
        int gid = 1;
        String groupName = "testgroup";
        AccountTO accountTO = new AccountTO(makeAccountMock(uid));
        GroupTO groupTO = new GroupTO(makeGroupMock(gid, groupName));

        when(accountDao.get(uid)).thenReturn(makeAccountMock(uid));
        when(groupDao.save(any())).then(invocationOnMock -> {
            Group g = invocationOnMock.getArgument(0);
            g.setId(gid);
            return g;
        });

        GroupTO newGroup = service.createGroup(groupTO, accountTO);
        assertEquals(groupTO.getId(), newGroup.getId());
    }

    @Test(expected = AccountNotFoundException.class)
    public void testCreateGroupNonExistedUser() throws Exception {
        int uid = 7;
        when(accountDao.get(uid)).thenReturn(null);

        service.createGroup(new GroupTO(makeGroupMock(1, "testgroup")), new AccountTO(makeAccountMock(uid)));
    }

    @Test
    public void testUpdateGroup() throws Exception {
    }

    @Test
    public void testGetMemberRole() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    private Group makeGroupMock(int id, String text) {
        Group group = new Group(text, text, text);
        group.setId(id);
        return group;
    }

    private Account makeAccountMock(Integer id) {
        Account a = new Account();
        a.setId(id);
        a.setName("Test account");
        a.setLogin("test_acc");
        return a;
    }

}