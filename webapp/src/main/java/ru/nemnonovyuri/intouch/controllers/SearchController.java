package ru.nemnonovyuri.intouch.controllers;

import ru.nemnonovyuri.intouch.dto.AccountTO;
import ru.nemnonovyuri.intouch.dto.GroupTO;
import ru.nemnonovyuri.intouch.service.AccountService;
import ru.nemnonovyuri.intouch.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ru.nemnonovyuri.intouch.helpers.AccountHelper;
import ru.nemnonovyuri.intouch.util.StringUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static ru.nemnonovyuri.intouch.helpers.GroupHelper.getGroupImageUrl;
import static ru.nemnonovyuri.intouch.helpers.GroupHelper.getGroupUrl;

@Controller
@RequestMapping("/search")
public class SearchController extends BaseController {
    @Autowired
    AccountService accountService;
    @Autowired
    GroupService groupService;

    private static Logger log = LoggerFactory.getLogger(ProfileController.class);
    private static int itemsPerPage = 3;

    @RequestMapping
    public ModelAndView showSearchPage(String name) {
        log.debug("showSearchPage({})", name);
        ModelAndView model = new ModelAndView("search_results");
        model.addObject("searchName", name);
        return model;
    }

    @RequestMapping(value = "/ajax/users", produces = "application/json")
    @ResponseBody
    public PaginatedSearchResult doAjaxUserSearch(String name, @RequestParam(required = false, defaultValue = "1") Integer page) {
        log.debug("doAjaxUserSearch({}, {})", name, page);
        Integer resultCount = accountService.getSearchByNameResultCount(name);
        PaginatedSearchResult result = new PaginatedSearchResult(resultCount);
        if (resultCount > 0) {
            List<AccountTO> userList = accountService.getByNamePaginated(name, page, itemsPerPage);
            for (AccountTO to : userList) {
                result.addItem(makeAccountSearchResult(to));
            }
        }
        log.debug("found {} users)", resultCount);
        return result;
    }

    @RequestMapping(value = "/ajax/groups", produces = "application/json")
    @ResponseBody
    public PaginatedSearchResult doAjaxGroupSearch(String title, @RequestParam(required = false, defaultValue = "1") Integer page) {
        log.debug("doAjaxGroupSearch({}, {})", title, page);
        Integer resultCount = groupService.getSearchByTitleResultCount(title);
        PaginatedSearchResult result = new PaginatedSearchResult(resultCount);
        if (resultCount > 0) {
            List<GroupTO> groupList = groupService.getByTitlePaginated(title, page, itemsPerPage);
            for (GroupTO to : groupList) {
                result.addItem(makeGroupSearchResult(to));
            }
        }
        log.debug("found {} groups)", resultCount);
        return result;
    }

    @RequestMapping(value = "/ajax", produces = "application/json")
    @ResponseBody
    public List<SearchResult> doAjaxSearch(String name) {
        log.debug("doAjaxSearch({})", name);
        List<SearchResult> results = new LinkedList<>();
        for (AccountTO user : getUsersByName(name)) {
            results.add(makeAccountSearchResult(user));
        }
        for (GroupTO group : getGroupsByTitle(name)) {
            results.add(makeGroupSearchResult(group));
        }
        return results;
    }

    private List<AccountTO> getUsersByName(String name) {
        log.debug("getUsersByName({})", name);
        return StringUtils.isEmpty(name) ? accountService.getAll() : accountService.getByName(name);
    }

    private List<GroupTO> getGroupsByTitle(String title) {
        log.debug("getGroupsByTitle({})", title);
        return StringUtils.isEmpty(title) ? groupService.getAll() : groupService.getByTitle(title);
    }

    private SearchResult makeAccountSearchResult(AccountTO acc) {
        return new SearchResult(acc.getId(), acc.getName(), AccountHelper.getAccountProfileUrl(acc), AccountHelper.getAccountPhotoUrl(acc));
    }

    private SearchResult makeGroupSearchResult(GroupTO group) {
        return new SearchResult(group.getId(), group.getTitle(), getGroupUrl(group), getGroupImageUrl(group));
    }

    private class SearchResult {
        private Integer id;
        private String name;
        private String url;
        private String imgUrl;

        public SearchResult(Integer id, String name, String url, String imgUrl) {
            this.id = id;
            this.name = name;
            this.url = url;
            this.imgUrl = imgUrl;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }
    }

    private class PaginatedSearchResult {
        private Integer pageCount;
        private List<SearchResult> items;

        public PaginatedSearchResult(Integer itemsCount) {
            pageCount = (int) Math.ceil(itemsCount * 1.0 / itemsPerPage);
            items = new ArrayList<>();
        }

        public void addItem(Integer id, String name, String url, String imageUrl) {
            items.add(new SearchResult(id, name, url, imageUrl));
        }

        public void addItem(SearchResult item) {
            items.add(item);
        }

        public Integer getPageCount() {
            return pageCount;
        }

        public List<SearchResult> getItems() {
            return items;
        }
    }
}
