<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@page import="AccountHelper" %>--%>
<%--<%@page import="GroupHelper" %>--%>
<jsp:include page="header.jsp"/>
<div class="col-md-4 col-md-offset-4">
    <div class="panel panel-default" id="user-list">
        <div class="panel-heading">
            <h4 class="panel-title">Users</h4>
        </div>
        <div class="panel-body">

        </div>
    </div>
    <div class="panel panel-default" id="group-list">
        <div class="panel-heading">
            <h4 class="panel-title">Groups</h4>
        </div>
        <div class="panel-body">

        </div>
    </div>
</div>
<jsp:include page="footer.jsp"/>
<script>
    var searchName = "${searchName}";
    // templates
    var listStart = "<ul class='media-list'>";
    var listItem = "<li class='media'>" +
            "<div class='media-left'><a href='%url%'><img class='media-object' src='%imageUrl%' width='64'></a></div>" +
            "<div class='media-body'><h4 class='media-heading'><a href='%url%'>%title%</a></h4></div>" +
            "</li>";
    var listEnd = "</ul>";
    $("#user-list").on('click', '.page-number', function () {
        getUserSearchResults($(this).children().text());
    });
    $("#group-list").on('click', '.page-number', function () {
        getUserSearchResults($(this).children().text());
    });
    getUserSearchResults(1);
    getGroupSearchResults(1);

    function getUserSearchResults(page) {
        getSearchResults("search/ajax/users", {name: searchName, page: page}, "Nobody was found", "#user-list");
    }

    function getGroupSearchResults(page) {
        getSearchResults("search/ajax/groups", {title: searchName, page: page}, "Groups was not found", "#group-list");
    }

    function getSearchResults(url, queryParams, emptyRes, listSelector) {
        $.get(url, queryParams, function (res) {
            var itemCount = res.items.length;
            var panelBody = $(listSelector).children(".panel-body");
            panelBody.empty();
            if (itemCount == 0) {
                panelBody.html("<p class='text-info'>" + emptyRes + "</p>");
            } else {
                var items = res.items;
                var s = listStart;
                for (var i = 0; i < itemCount; i++) {
                    s += listItem.replace("%url%", items[i].url)
                            .replace("%url%", items[i].url)
                            .replace("%imageUrl%", items[i].imgUrl)
                            .replace("%title%", items[i].name);
                }
                panelBody.append(s + listEnd);
                if (res.pageCount > 1) {
                    s = "<nav><ul class='pagination'>";
                    var p = 0;
                    while (p++ < res.pageCount) {
                        s += "<li class='page-number'><span class='btn btn-default'>" + p + "</span></li>";
                    }
                    panelBody.append(s + "</ul></nav>");
                }
            }
        });
    }
</script>