package ru.nemnonovyuri.intouch.test;

import javax.persistence.*;

@Entity
@Table(name = "contact_info")
public class ContactTest {
    @Id
    private Integer id;

    private Integer type;

    private String value;

    @ManyToOne
    @JoinColumn(name = "acc_id")
    private AccountTest account;
}

