package ru.nemnonovyuri.intouch.dto;

import ru.nemnonovyuri.intouch.model.Group;

public class GroupTO extends IdentityTO {
    private String name;
    private String description;
    private String title;
    private String photo;

    public GroupTO() {
    }

    public GroupTO(String name, String description, String title) {
        this(null, name, description, title, null);
    }

    public GroupTO(Group group) {
        this(group.getId(), group.getName(), group.getDescription(), group.getTitle(), group.getPhoto());
    }

    public GroupTO(Integer id, String name, String description, String title, String photo) {
        super(id);
        this.name = name;
        this.description = description;
        this.title = title;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        GroupTO groupTO = (GroupTO) o;

        if (!getName().equals(groupTO.getName())) return false;
        if (!getDescription().equals(groupTO.getDescription())) return false;
        if (!getTitle().equals(groupTO.getTitle())) return false;
        return getPhoto() != null ? getPhoto().equals(groupTO.getPhoto()) : groupTO.getPhoto() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getDescription().hashCode();
        result = 31 * result + getTitle().hashCode();
        result = 31 * result + (getPhoto() != null ? getPhoto().hashCode() : 0);
        return result;
    }
}
