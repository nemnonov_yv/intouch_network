package ru.nemnonovyuri.intouch.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "groups")
//@Access(value = AccessType.PROPERTY)
public class Group extends AbstractIdentity {
    private String name;
    private String description;
    private String title;
    private String photo;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "membership.group")
    private Set<GroupMember> members;

    public Group() {
    }

    public Group(String name, String title, String description) {
        this.name = name;
        this.title = title;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Set<GroupMember> getMembers() {
        return members;
    }

    public void setMembers(Set<GroupMember> members) {
        this.members = members;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        return getName().equals(((Group) o).getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
