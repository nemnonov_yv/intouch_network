<form method="post" action="/login">
    <label for="login">Login</label>
    <input type="text" name="login" class="form-control" id="login" value="${login}" required autocomplete="off">
    <label for="password">Password</label>
    <input type="password" name="password" class="form-control" id="password" required>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="remember"> Remember me
        </label>
    </div>
    <button type="submit" class="btn btn-default">Log in</button>
</form>
