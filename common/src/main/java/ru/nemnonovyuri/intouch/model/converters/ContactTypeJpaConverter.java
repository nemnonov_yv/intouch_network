package ru.nemnonovyuri.intouch.model.converters;

import ru.nemnonovyuri.intouch.model.ContactType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ContactTypeJpaConverter implements AttributeConverter<ContactType, Integer> {
    @Override
    public Integer convertToDatabaseColumn(ContactType type) {
        return type.getNumeric();
    }

    @Override
    public ContactType convertToEntityAttribute(Integer integer) {
        return ContactType.valueOf(integer);
    }
}
