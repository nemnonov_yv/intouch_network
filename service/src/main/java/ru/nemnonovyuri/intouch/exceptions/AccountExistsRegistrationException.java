package ru.nemnonovyuri.intouch.exceptions;

public class AccountExistsRegistrationException extends ServiceException {
    public AccountExistsRegistrationException() {
        super();
    }

    public AccountExistsRegistrationException(String message) {
        super(message);
    }
}
