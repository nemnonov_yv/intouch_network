package ru.nemnonovyuri.intouch.helpers;

import ru.nemnonovyuri.intouch.dto.GroupTO;

public class GroupHelper {
    public static String getGroupImageUrl(GroupTO gr) {
        return gr.getPhoto() == null ? "/static/i/group.png" : ImageUploads.getImageUrl(gr, gr.getPhoto(), ImageUploads.Type.GROUP_IMAGE);
    }

    public static String getGroupUrl(GroupTO group) {
        return "/groups/view/" + group.getId();
    }
}
