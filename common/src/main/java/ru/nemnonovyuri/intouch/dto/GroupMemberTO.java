package ru.nemnonovyuri.intouch.dto;

import ru.nemnonovyuri.intouch.model.MemberRole;

public class GroupMemberTO {
    private Integer accountId;
    private Integer groupId;
    private MemberRole role;

    public GroupMemberTO() {
    }

    public GroupMemberTO(Integer accountId, Integer groupId, MemberRole role) {
        this.accountId = accountId;
        this.groupId = groupId;
        this.role = role;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public MemberRole getRole() {
        return role;
    }

    public void setRole(MemberRole role) {
        this.role = role;
    }
}
