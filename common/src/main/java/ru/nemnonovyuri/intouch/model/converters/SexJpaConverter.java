package ru.nemnonovyuri.intouch.model.converters;

import ru.nemnonovyuri.intouch.model.Sex;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class SexJpaConverter implements AttributeConverter<Sex, Integer> {
    @Override
    public Integer convertToDatabaseColumn(Sex sex) {
        return sex.getNumeric();
    }

    @Override
    public Sex convertToEntityAttribute(Integer integer) {
        return Sex.valueOf(integer);
    }
}
