</div></div>
<script>
    $.ready(
            $("#header-searchbox").autocomplete({
                minLength: 2,
                source: function (request, response) {
                    $.get({
                        url: "/search/ajax",
                        data: {
                            name: request.term
                        },
                        success: function (data) {
                            var res = $.map(data, function (item, i) {
                                return {value: item.id, label: item.name, url: item.url};
                            });
                            response(res);
                        }
                    });
                },
                select: function (event, ui) {
                    $("#header-searchbox").val(ui.item.label);
                    window.location.href = ui.item.url;
                }
            })
    );
</script>
</body>
</html>
