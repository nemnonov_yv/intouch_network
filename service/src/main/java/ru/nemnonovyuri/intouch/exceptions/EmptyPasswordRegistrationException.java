package ru.nemnonovyuri.intouch.exceptions;

public class EmptyPasswordRegistrationException extends ServiceException {
    public EmptyPasswordRegistrationException() {
        super();
    }

    public EmptyPasswordRegistrationException(String message) {
        super(message);
    }

    public EmptyPasswordRegistrationException(Throwable cause) {
        super(cause);
    }

    public EmptyPasswordRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }
}
