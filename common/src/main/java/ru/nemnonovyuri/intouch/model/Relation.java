package ru.nemnonovyuri.intouch.model;

//@Entity
//@Table(name="relations", uniqueConstraints = {@UniqueConstraint(columnNames = {"acc1_id", "acc2_id"})})
public class Relation {
    private Account first;
    private Account second;

    public Relation(Account first, Account second) {
        this.first = first;
        this.second = second;
    }

    public Relation() {
    }

    public Account getFirst() {
        return first;
    }

    public void setFirst(Account first) {
        this.first = first;
    }

    public Account getSecond() {
        return second;
    }

    public void setSecond(Account second) {
        this.second = second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Relation relation = (Relation) o;

        return getFirst().equals(relation.getFirst()) && getSecond().equals(relation.getSecond());
    }

    @Override
    public int hashCode() {
        int result = getFirst().hashCode();
        result = 31 * result + getSecond().hashCode();
        return result;
    }
}
