package ru.nemnonovyuri.intouch.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import ru.nemnonovyuri.intouch.model.ContactInfo;
import ru.nemnonovyuri.intouch.model.ContactType;

@JacksonXmlRootElement(localName = "contact")
public class ContactTO extends IdentityTO {
    private ContactType type;
    private String value;

    public ContactTO() {
    }

    public ContactTO(ContactType type, String value) {
        this(null, type, value);
    }

    public ContactTO(Integer id, ContactType type, String value) {
        super(id);
        this.type = type;
        this.value = value;
    }

    public ContactTO(ContactInfo info) {
        this(info.getId(), info.getType(), info.getValue());
    }

    public ContactType getType() {
        return type;
    }

    public void setType(ContactType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContactTO contactTO = (ContactTO) o;

        if (getId() != null && !getId().equals(contactTO.getId())) return false;
        if (getType() != contactTO.getType()) return false;
        return getValue().equals(contactTO.getValue());
    }

    @Override
    public int hashCode() {
        int result = getId() == null ? 0 : getId();
        result = 31 * result + getType().hashCode();
        result = 31 * result + getValue().hashCode();
        return result;
    }
}
