package ru.nemnonovyuri.intouch.dao;

import ru.nemnonovyuri.intouch.model.Account;
import ru.nemnonovyuri.intouch.model.FriendshipRequest;
import org.springframework.stereotype.Repository;

@Repository
public class FriendshipRequestDao extends JpaDao<FriendshipRequest> {
    public FriendshipRequest getByAccounts(Account sender, Account recipient) {
        return (FriendshipRequest) entityManager
                .createQuery("SELECT r FROM FriendshipRequest r WHERE r.sender=:sender AND r.recipient=:recipient")
                .setParameter("sender", sender)
                .setParameter("recipient", recipient)
                .getSingleResult();
    }
}
