package ru.nemnonovyuri.intouch.model.converters;

import ru.nemnonovyuri.intouch.model.AccountStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class AccountStatusJpaConverter implements AttributeConverter<AccountStatus, Integer> {
    @Override
    public Integer convertToDatabaseColumn(AccountStatus status) {
        return status.getNumeric();
    }

    @Override
    public AccountStatus convertToEntityAttribute(Integer integer) {
        return AccountStatus.valueOf(integer);
    }
}
