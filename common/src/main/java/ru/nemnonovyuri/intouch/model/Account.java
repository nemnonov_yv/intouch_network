package ru.nemnonovyuri.intouch.model;

import ru.nemnonovyuri.intouch.model.converters.AccountStatusJpaConverter;
import ru.nemnonovyuri.intouch.model.converters.SexJpaConverter;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "accounts")
//@Access(value = AccessType.PROPERTY)
public class Account extends AbstractIdentity {
    private String login;

    private String name;

    @Column(name = "birthdate")
    private Date birthDate;

    @Convert(converter = SexJpaConverter.class)
    private Sex sex;

    @Column(name = "pass")
    private String passwordHash;

    private String photo;

    @Convert(converter = AccountStatusJpaConverter.class)
    private AccountStatus status;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ContactInfo> contacts;

    @ManyToMany
    @JoinTable(name = "relations",
            joinColumns = @JoinColumn(name = "acc1_id"),
            inverseJoinColumns = @JoinColumn(name = "acc2_id"))
    private Set<Account> friends;

    @ManyToMany(mappedBy = "friends")
    private Set<Account> relations;

    @OneToMany(mappedBy = "membership.member", fetch = FetchType.LAZY)
    private Set<GroupMember> groups;

    @OneToMany(mappedBy = "recipient")
    private Set<FriendshipRequest> incomeRequests;

    @OneToMany(mappedBy = "sender")
    private Set<FriendshipRequest> outcomeRequests;

    public Account() {
        contacts = new HashSet<>();
        friends = new HashSet<>();
        groups = new HashSet<>();
        status = AccountStatus.ACTIVE;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        return (getLogin().equals(((Account) o).getLogin()));
    }

    @Override
    public int hashCode() {
        int result = getLogin().hashCode();
        result = 31 * result + getName().hashCode();
        return result;
    }

    public Set<ContactInfo> getContacts() {
        return contacts;
    }

    public void setContacts(Set<ContactInfo> friends) {
        this.contacts = friends;
    }

    //
    public Set<Account> getFriends() {
        return friends;
    }

    public void setFriends(Set<Account> friends) {
        this.friends = friends;
    }

    public Set<GroupMember> getGroups() {
        return groups;
    }

    public void setGroups(Set<GroupMember> groups) {
        this.groups = groups;
    }

    public Set<FriendshipRequest> getIncomeRequests() {
        return incomeRequests;
    }

    public void setIncomeRequests(Set<FriendshipRequest> incomeRequests) {
        this.incomeRequests = incomeRequests;
    }

    public Set<FriendshipRequest> getOutcomeRequests() {
        return outcomeRequests;
    }

    public void setOutcomeRequests(Set<FriendshipRequest> outcomeRequests) {
        this.outcomeRequests = outcomeRequests;
    }
}
