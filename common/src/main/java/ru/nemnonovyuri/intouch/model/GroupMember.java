package ru.nemnonovyuri.intouch.model;

import javax.persistence.*;

@Entity
@Table(name = "group_members"/*, uniqueConstraints = {
        @UniqueConstraint(columnNames = {"acc_id", "group_id"})
}*/)
@AssociationOverrides({
        @AssociationOverride(name = "membership.member", joinColumns = @JoinColumn(name = "acc_id")),
        @AssociationOverride(name = "membership.group", joinColumns = @JoinColumn(name = "group_id")),
})
public class GroupMember {
    @EmbeddedId
    private GroupMembership membership = new GroupMembership();

    public GroupMember() {
    }

    public GroupMember(Account acc, Group gr, MemberRole role) {
        this(new GroupMembership(acc, gr, role));
    }

    public GroupMember(GroupMembership membership) {
        this.membership = membership;
    }

    public GroupMembership getMembership() {
        return membership;
    }

    public void setMembership(GroupMembership gm) {
        this.membership = gm;
    }

    @Transient
    public Account getMember() {
        return getMembership().getMember();
    }

    public void setMember(Account member) {
        getMembership().setMember(member);
    }

    @Transient
    public Group getGroup() {
        return getMembership().getGroup();
    }

    public void setGroup(Group group) {
        getMembership().setGroup(group);
    }
}
