package ru.nemnonovyuri.intouch;

import ru.nemnonovyuri.intouch.model.*;

import java.sql.Date;

public class DaoTestData {
    public static final Account ACCOUNT_ID1 = makeAccount(1, "andy", "pass1", "Andrey", Date.valueOf("1980-02-15"), Sex.MALE, AccountStatus.ACTIVE);
    public static final Account ACCOUNT_ID2 = makeAccount(2, "gbmps", "pass2", "Giuseppe Benedetto Maria Placido di Savoia", Date.valueOf("1766-10-05"), Sex.MALE, AccountStatus.DELETED);
    public static final Account ACCOUNT_ID3 = makeAccount(3, "mary", "masha", "Maria Ivanovna", Date.valueOf("1975-06-11"), Sex.FEMALE, AccountStatus.ACTIVE);
    public static final Account ACCOUNT_ID4 = makeAccount(4, "tai", "tai", "Alex", null, Sex.UNSPECIFIED, AccountStatus.ACTIVE);
    public static final Account NEW_ACCOUNT = makeAccount(4, "Vasya", "$$$VasyaPassword$$$", "Vasiliy Pupkin", Date.valueOf("2000-01-01"), Sex.MALE, AccountStatus.ACTIVE);

    public static final ContactInfo CONTACT_INFO_ID1 = new ContactInfo(ACCOUNT_ID1, ContactType.PERSONAL_PHONE, "+7-999-888-77-66");
    public static final ContactInfo CONTACT_INFO_ID2 = new ContactInfo(ACCOUNT_ID2, ContactType.ICQ, "123-999-789");
    public static final ContactInfo CONTACT_INFO_ID3 = new ContactInfo(ACCOUNT_ID1, ContactType.EMAIL, "dont@mail.me");

    public static final Group GROUP_ID1 = makeGroup(1, "funnycats", "Funny pretty cats", "Cats you will love");
    public static final Group GROUP_ID2 = makeGroup(2, "smartydogs", "Smarty dogs", "Cat fan restricted");
    public static final Group GROUP_ID3 = makeGroup(3, "hollywars", "Virtual battles", "Lets mortal combat begin");

    public static final Relation REL_ACC1_ACC2 = new Relation(ACCOUNT_ID1, ACCOUNT_ID2);
    public static final Relation REL_ACC1_ACC3 = new Relation(ACCOUNT_ID1, ACCOUNT_ID3);
    public static final Relation REL_ACC3_ACC4 = new Relation(ACCOUNT_ID3, ACCOUNT_ID4);

    private static Account makeAccount(int id, String login, String pass, String name, Date birthdate, Sex sex, AccountStatus status) {
        Account acc = new Account();
        acc.setId(id);
        acc.setLogin(login);
        acc.setName(name);
        acc.setPasswordHash(pass);
        acc.setBirthDate(birthdate);
        acc.setSex(sex);
        acc.setStatus(status);
        return acc;
    }

    private static Group makeGroup(int id, String name, String title, String description) {
        Group gr = new Group(name, title, description);
        gr.setId(id);
        return gr;
    }
}
