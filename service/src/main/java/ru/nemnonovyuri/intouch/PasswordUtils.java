package ru.nemnonovyuri.intouch;

import ru.nemnonovyuri.intouch.model.Account;

public class PasswordUtils {
    public static String generateHash(String password) {
        return "Hash$" + password;
    }

    public static boolean isPasswordCorrect(Account acc, String password) {
        return generateHash(password).equals(acc.getPasswordHash());
    }
}
