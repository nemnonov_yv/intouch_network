package ru.nemnonovyuri.intouch.model.converters;

import ru.nemnonovyuri.intouch.model.MemberRole;

import javax.persistence.AttributeConverter;

public class GroupMemberJpaConverter implements AttributeConverter<MemberRole, Integer> {
    @Override
    public Integer convertToDatabaseColumn(MemberRole role) {
        return role.getNumeric();
    }

    @Override
    public MemberRole convertToEntityAttribute(Integer integer) {
        return MemberRole.valueOf(integer);
    }
}
