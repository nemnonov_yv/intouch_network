package ru.nemnonovyuri.intouch.controllers;

import ru.nemnonovyuri.intouch.dto.AccountTO;
import ru.nemnonovyuri.intouch.exceptions.NotFoundException;
import ru.nemnonovyuri.intouch.helpers.AccountHelper;
import ru.nemnonovyuri.intouch.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/users")
public class UsersController extends BaseController {
    @Autowired
    AccountService accountService;

    private static final Logger log = LoggerFactory.getLogger(UsersController.class);

    @RequestMapping("/view/{id}")
    public ModelAndView view(@PathVariable("id") Integer id) {
        log.debug("view({})", id);
        AccountTO user = accountService.getAccount(id);
        if (user == null) {
            log.debug("user not found");
            throw new NotFoundException();
        }
        ModelAndView model = new ModelAndView("user_page");
        model.addObject("user", user);
        model.addObject("contacts", accountService.getContactInfo(user).getContacts());
        model.addObject("photoUrl", AccountHelper.getAccountPhotoUrl(user));
        setTitle(model, user.getName());
        return model;
    }
}
