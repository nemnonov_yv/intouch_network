package ru.nemnonovyuri.intouch.test;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "accounts")
public class AccountTest {
    @Id
    private Integer id;

    private String name;

    private String login;

    @OneToMany(mappedBy = "account")
    private Set<ContactTest> contacts;
}
