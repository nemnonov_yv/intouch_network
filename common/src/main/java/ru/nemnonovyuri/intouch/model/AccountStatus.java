package ru.nemnonovyuri.intouch.model;

public enum AccountStatus {
    ACTIVE(0),
    DELETED(1);

    private int numeric;

    AccountStatus(int numeric) {
        this.numeric = numeric;
    }

    public static AccountStatus valueOf(int numeric) {
        for (AccountStatus status : AccountStatus.values()) {
            if (status.getNumeric() == numeric) {
                return status;
            }
        }
        throw new RuntimeException("Invalid status numeric presentation");
    }

    public int getNumeric() {
        return numeric;
    }
}
