package ru.nemnonovyuri.intouch.service;

import ru.nemnonovyuri.intouch.dao.AccountDao;
import ru.nemnonovyuri.intouch.dto.AccountTO;
import ru.nemnonovyuri.intouch.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SecurityService extends BaseService {
    @Autowired
    AccountDao accountDao;

    public SecurityService() {
    }

    public String getToken(AccountTO to) {
        return to.getId() + "_token";
    }

    public AccountTO getAccountByToken(String token) {
        int id = Integer.valueOf(token.split("_")[0]);
        Account account = accountDao.get(id);
        return account != null ? new AccountTO(account) : null;
    }
}
