# InTouch Social Network 

** Functionality: **

+ registration  
+ authentication  
+ ajax search with pagination  
+ display profile  
+ edit profile  
+ upload and download avatar  
+ users export to xml  

** Tools: **  
JDK 8, Spring 4, JPA 2 / Hibernate 4, Jackson, jQuery 2, Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, Logback, Git / Bitbucket, Tomcat 8, MySQL, IntelliJIDEA 2016.1

** Notes: **  
SQL ddl is located in the `db/ddl.sql`

** Demo: **
[https://intouch-app-demo.herokuapp.com](https://intouch-app-demo.herokuapp.com)

login: test

password: 1

__  
**Nemnonov Yuri**