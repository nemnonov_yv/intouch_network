package ru.nemnonovyuri.intouch.util;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtils {
    public static Date parseString(String ddmmyy) throws ParseException {
        return new Date((new SimpleDateFormat("dd.MM.yyyy").parse(ddmmyy)).getTime());
    }

    public static String convertToString(Date date) {
        return new SimpleDateFormat("dd.MM.yyyy").format(date);
    }
}
