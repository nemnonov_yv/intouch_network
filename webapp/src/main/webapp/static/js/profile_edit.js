function ContactFormHandler() {
    /* selectors */
    var formSelector = '#contacts-form';
    var lastRowSelector = formSelector + ' .row:last';
    var addBtnSelector = '#add-row';
    var removeBtnSelector = 'div[class$=remove-contact]';
    var inputIdSelector = 'input[name=id]';
    var inputTypeSelector = 'select[name=type]';
    var inputValueSelector = 'input[name=value]';
    var inputDeletedSelector = 'input[name=delete]';
    var confirmBtnSelector = '#confirm-dlg-save-btn';
    var phoneValidationPattern = /\+(\d{1,3})\((\d{3})\)(\d{5,7})/;

    /* contact entry add & remove */
    var rowTemplate = $(lastRowSelector).clone();

    $(removeBtnSelector).on('click', function () {
        var row = $(this).parent().parent();
        $(row).children(inputDeletedSelector).val(1);
        $(row).hide();
    });

    $(addBtnSelector).on('click', function () {
        rowTemplate.clone().insertAfter(lastRowSelector);
    });

    /* save */
    $(confirmBtnSelector).on('click', function () {
        $(formSelector).submit();
    });

    /* contact validation */
    $(inputValueSelector).change(function () {
        var input = $(this);
        var row = input.parent().parent().parent();
        if (isPhoneContact(row.find(inputTypeSelector))) {
            if (!phoneValidationPattern.test(input.val())) {
                addError(input);
                return;
            }
        }
        resetError(input);
    });

    $(inputTypeSelector).change(function () {
        var row = $(this).parent().parent().parent();
        var contactVal = row.find(inputValueSelector);
        if (contactVal.val() != '' && isPhoneContact(this)) {
            if (!phoneValidationPattern.test(contactVal.val())) {
                addError(contactVal);
                return;
            }
        }
        resetError(contactVal);
    });

    function addError(input) {
        input.parent().addClass("has-error");
        input.popover({
            container: 'body',
            content: 'Value must meet next pattern +12(123)1234567',
            trigger: 'manual'
        });
        input.popover('show');
        $(formSelector).find('button.btn-primary').prop('disabled', true);
    }

    function resetError(input) {
        input.parent().removeClass("has-error");
        input.popover('destroy');
        $(formSelector).find('button.btn-primary').prop('disabled', false);
    }

    function isPhoneContact(select) {
        var val = $(select).val();
        return val == 1 || val == 2;
    }
}
