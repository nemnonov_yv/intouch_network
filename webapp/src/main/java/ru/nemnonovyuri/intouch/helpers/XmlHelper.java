package ru.nemnonovyuri.intouch.helpers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import ru.nemnonovyuri.intouch.dto.AccountInfoTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

public class XmlHelper {
    public static String getXml(AccountInfoTO accountInfo) throws JsonProcessingException {
        XmlMapper mapper = new XmlMapper();
        mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        return mapper.writeValueAsString(accountInfo);
    }

    public static AccountInfoTO getFromXml(MultipartFile file) {
        StringBuilder sb = new StringBuilder();
        XmlMapper mapper = new XmlMapper();
        AccountInfoTO infoTO = new AccountInfoTO();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            while (reader.ready()) {
                sb.append(reader.readLine());
            }
            infoTO = mapper.readValue(sb.toString(), AccountInfoTO.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return infoTO;
    }
}
