package ru.nemnonovyuri.intouch.interceptors;

import ru.nemnonovyuri.intouch.dto.AccountTO;
import ru.nemnonovyuri.intouch.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class AuthInterceptor implements HandlerInterceptor {
    //    private static final List<String> skipUrls = Arrays.asList("/", "/signup", "/login", "/logout");
    private static final String TOKEN = "token";

    @Autowired
    private SecurityService securityService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        HttpSession session = request.getSession();
        String uri = request.getRequestURI();

        if (isLoggedIn(session) || authByCookieSucceed(request, response) || authNotNeeded(uri)) {
            return true;
        } else {
            response.sendRedirect("/logout");
        }
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object o, Exception e) throws Exception {

    }

    private boolean isLoggedIn(HttpSession session) {
        return getAccountFromSession(session) != null;
    }

    private AccountTO getAccountFromSession(HttpSession session) {
        return (AccountTO) session.getAttribute("account");
    }

    private boolean authNotNeeded(String uri) {
        return false;
    }

    private boolean authByCookieSucceed(HttpServletRequest request, HttpServletResponse response) {
        for (Cookie cookie : request.getCookies()) {
            if (cookie.getName().equals(TOKEN)) {
                AccountTO account = securityService.getAccountByToken(cookie.getValue());
                if (account != null) {
                    request.getSession().setAttribute("account", account);
                    cookie.setMaxAge(3600);
                    response.addCookie(cookie);
                    return true;
                }
            }
        }
        return false;
    }

}
