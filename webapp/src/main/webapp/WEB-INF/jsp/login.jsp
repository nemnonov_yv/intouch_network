<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="header.jsp"/>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <c:if test="${failed}">
            <h3 class="text-danger">Incorrect credentials.</h3>

            <p>Try again or <a href="<c:url value="/signup"/>">register</a></p>
        </c:if>
        <jsp:include page="login_form.jsp"/>
    </div>
</div>
<jsp:include page="footer.jsp"/>