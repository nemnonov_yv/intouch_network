package ru.nemnonovyuri.intouch.dao;

import ru.nemnonovyuri.intouch.model.Account;
import ru.nemnonovyuri.intouch.model.Group;
import ru.nemnonovyuri.intouch.model.GroupMember;
import ru.nemnonovyuri.intouch.model.MemberRole;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static ru.nemnonovyuri.intouch.DaoTestData.*;
import static ru.nemnonovyuri.intouch.model.MemberRole.MODERATOR;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-test-context.xml"})
public class GroupDaoTest {

    @Autowired
    private GroupDao dao;
    @Autowired
    private AccountDao accountDao;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testSave() throws Exception {
        Group gr = new Group();
        gr.setName("test-group");
        gr.setTitle("Test group");
        gr.setDescription("Just a test");
        Group saved = dao.save(gr);
        assertEquals(gr, dao.get(saved.getId()));
    }

    @Test
    public void testGet() throws Exception {
        assertEquals(GROUP_ID1, dao.get(1));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testUpdate() throws Exception {
        Group gr = GROUP_ID2;
        gr.setDescription("Love dogs, hate cats");
        dao.save(gr);
        assertEquals(gr.getDescription(), dao.get(gr.getId()).getDescription());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testDelete() throws Exception {
        int deleteId = 2;
        Group gr = dao.get(deleteId);
        if (gr == null) {
            fail("Group with id=" + deleteId + " doesn't exists");
        }
        dao.delete(gr);
        assertEquals(dao.get(deleteId), null);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Group> all = new ArrayList<>();
        all.add(GROUP_ID1);
        all.add(GROUP_ID2);
        all.add(GROUP_ID3);
        List<Group> all1 = dao.getAll();
        assertEquals(all, all1);
    }

    @Test
    public void testGetByTitle() throws Exception {
        List<Group> expected = Arrays.asList(GROUP_ID2, GROUP_ID3);
        assertEquals(expected, dao.getByTitle("rt"));
    }

    @Test
    public void testGetQueryByTitleResultCount() throws Exception {
        assertEquals(2L, (long) dao.getQueryByTitleResultCount("ty"));
    }

    @Test
    @Transactional
    public void testGetWithMembers() throws Exception {
        Set<Account> members = new HashSet<>();
        members.add(ACCOUNT_ID1);
        members.add(ACCOUNT_ID3);
        assertEquals(members, getGroupMembers(dao.get(GROUP_ID1.getId())));
    }

    @Test
    public void testGetMembersByRole() throws Exception {
        List<Account> members = new ArrayList<>();
        members.add(ACCOUNT_ID3);
        assertEquals(members, dao.getMembersByRole(GROUP_ID1, MemberRole.ADMIN));
    }

    @Test
    public void testGetMemberRole() throws Exception {
        assertEquals(MemberRole.ADMIN, dao.getMemberRole(GROUP_ID2, ACCOUNT_ID2));
    }

    @Test
    @Transactional
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testAddMember() throws Exception {
        Set<Account> expected = new HashSet<>();
        expected.add(ACCOUNT_ID1);
        expected.add(ACCOUNT_ID3);
        expected.add(ACCOUNT_ID4);
        dao.addMember(GROUP_ID1, ACCOUNT_ID4, MODERATOR);
        assertEquals(expected, getGroupMembers(dao.get(GROUP_ID1.getId())));
    }

    private Set<Account> getGroupMembers(Group gr) {
        return gr.getMembers().stream().map(GroupMember::getMember).collect(Collectors.toSet());
    }

    @After
    public void tearDown() throws Exception {
    }
}