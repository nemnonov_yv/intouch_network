package ru.nemnonovyuri.intouch.controllers;

import ru.nemnonovyuri.intouch.helpers.GroupHelper;
import ru.nemnonovyuri.intouch.helpers.ImageUploads;
import ru.nemnonovyuri.intouch.dto.AccountTO;
import ru.nemnonovyuri.intouch.dto.GroupTO;
import ru.nemnonovyuri.intouch.exceptions.ActionForbiddenException;
import ru.nemnonovyuri.intouch.exceptions.NotFoundException;
import ru.nemnonovyuri.intouch.formmodels.GroupForm;
import ru.nemnonovyuri.intouch.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/groups")
public class GroupController extends BaseController {
    @Autowired
    GroupService groupService;

    private static final Logger log = LoggerFactory.getLogger(GroupController.class);

    @RequestMapping("")
    public ModelAndView getAll() {
        log.debug("getAll()");
        List<GroupTO> groups = groupService.getAll();
        ModelAndView model = new ModelAndView("group_all");
        model.addObject("groupList", groups);
        return model;
    }

    @RequestMapping("/view/{id}")
    public ModelAndView view(@PathVariable("id") Integer id) {
        log.debug("view({})", id);
        return getViewModel("group_view", getIfExists(id));
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {
        log.debug("create()");
        ModelAndView model = getViewModel("group_edit", new GroupTO());
        model.addObject("action", "create");
        return model;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(GroupForm model, HttpServletRequest request) {
        log.debug("create({})", model);
        AccountTO account = getAccountFromSession(request);
        GroupTO created = groupService.createGroup(new GroupTO(model.getName(), model.getDescription(), model.getTitle()), account);
        String uploaded = ImageUploads.upload(model.getPhoto(), created, ImageUploads.Type.GROUP_IMAGE);
        if (uploaded != null) {
            created.setPhoto(uploaded);
            groupService.updateGroup(created);
        }
        return "redirect:view/" + created.getId();
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id") Integer id, HttpServletRequest request) {
        log.debug("edit({})", id);
        GroupTO group = getIfExists(id);
        checkUserPermissions(request, group);
        ModelAndView model = getViewModel("group_edit", group);
        model.addObject("action", "update");
        return model;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ModelAndView update(GroupForm model, HttpServletRequest request) {
        log.debug("update({})", model);
        GroupTO group = getIfExists(model.getId());
        checkUserPermissions(request, group);
        group.setTitle(model.getTitle());
        group.setName(model.getName());
        group.setDescription(model.getDescription());
        String uploaded = ImageUploads.upload(model.getPhoto(), group, ImageUploads.Type.GROUP_IMAGE);
        String oldPhoto = group.getPhoto();
        if (uploaded != null) {
            group.setPhoto(uploaded);
        }
        groupService.updateGroup(group);
        if (uploaded != null) {
            try {
                ImageUploads.delete(group, oldPhoto, ImageUploads.Type.GROUP_IMAGE);
            } catch (IOException e) {
                log.warn("Removing group photo error: {}, {}", oldPhoto, e.getMessage());
            }
        }
        return new ModelAndView("redirect:view/" + group.getId());
    }

    private void checkUserPermissions(HttpServletRequest request, GroupTO group) {
        log.debug("checkUserPermissions(request, {})", group);
        AccountTO account = getAccountFromSession(request);
        if (!groupService.hasUpdatePermission(group, account)) {
            log.info("User {} has no permissions to update {}", account, group);
            throw new ActionForbiddenException();
        }
    }

    private GroupTO getIfExists(int id) {
        log.debug("getIfExists({})", id);
        GroupTO group = groupService.getGroup(id);
        if (group == null) {
            log.info("Group not found by id: {}", id);
            throw new NotFoundException();
        }
        return group;
    }

    private ModelAndView getViewModel(String viewName, GroupTO group) {
        log.debug("getViewModel({}, {})", viewName, group);
        ModelAndView model = new ModelAndView(viewName);
        model.addObject("group", group);
        model.addObject("photoUrl", GroupHelper.getGroupImageUrl(group));
        return model;
    }
}
