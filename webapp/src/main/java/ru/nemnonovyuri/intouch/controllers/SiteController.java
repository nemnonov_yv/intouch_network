package ru.nemnonovyuri.intouch.controllers;

import ru.nemnonovyuri.intouch.dto.AccountTO;
import ru.nemnonovyuri.intouch.formmodels.SignupForm;
import ru.nemnonovyuri.intouch.model.Sex;
import ru.nemnonovyuri.intouch.service.AccountService;
import ru.nemnonovyuri.intouch.service.SecurityService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static ru.nemnonovyuri.intouch.util.StringUtils.isEmpty;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@SessionAttributes("account")
@RequestMapping("")
public class SiteController extends BaseController {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(SiteController.class);

    private static final int tokenMaxAge = 3600;
    private static final String TOKEN = "token";

    @Autowired
    private AccountService accountService;
    @Autowired
    private SecurityService securityService;

    @RequestMapping("/")
    public String home(@SessionAttribute(required = false) AccountTO account) {
        if (account == null) {
            log.debug("Show home page");
            return "index";
        }
        return "redirect:/profile";
    }

    @RequestMapping(value = "login", method = GET)
    public String showLoginForm() {
        log.debug("Show login page");
        return "login";
    }

    @RequestMapping(value = "login", method = POST)
    public ModelAndView doLogin(String login, String password, String remember, HttpServletResponse response) {
        log.info("Login attempt: login: {}, password: {}", login, password);
        AccountTO to = new AccountTO();
        to.setLogin(login);
        to.setPassword(password);
        AccountTO account = accountService.getIfCredentialsCorrect(to);
        if (account == null) {
            return loginFailed(to.getLogin());
        }
        ModelAndView view = new ModelAndView("redirect:/profile");
        view.addObject("account", account);
        if ("on".equals(remember)) {
            String token = securityService.getToken(account);
            response.addCookie(createTokenCookie(token));
        }
        log.info("User {} - login succeed", login);
        return view;
    }

    @RequestMapping("logout")
    public String logout(SessionStatus sessionStatus, HttpServletResponse response) {
        log.debug("User logout");
        sessionStatus.setComplete();
        response.addCookie(removeTokenCookie());
        return "redirect:/";
    }

    @RequestMapping(value = "signup", method = GET)
    public String showSignupForm() {
        log.debug("Show registration form");
        return "signup";
    }

    @RequestMapping(value = "signup", method = POST)
    public ModelAndView doSignup(SignupForm model, SessionStatus sessionStatus) {
        log.debug("Signup form data: {}", model);
        Map<String, String> errors = getSignupErrors(model);
        AccountTO acc = new AccountTO();
        acc.setName(model.getName());
        acc.setLogin(model.getLogin());
        acc.setPassword(model.getPassword());
        acc.setSex(Sex.valueOf(model.getSex()));
        if (errors.size() != 0) {
            ModelAndView view = new ModelAndView("signup");
            view.addObject("errors", errors);
            view.addObject("account", acc);
            view.addObject("email", model.getEmail());
            sessionStatus.setComplete();
            return view;
        } else {
            AccountTO registered = accountService.registerNewAccount(acc, model.getEmail());
            ModelAndView view = new ModelAndView("redirect:/profile");
            view.addObject("account", registered);
            return view;
        }
    }

    private ModelAndView loginFailed(String login) {
        log.info("Login with username {} failed", login);
        ModelAndView view = new ModelAndView("login");
        view.addObject("login", login);
        view.addObject("failed", true);
        return view;
    }

    private Cookie createTokenCookie(String value) {
        log.debug("Sending cookie value {}", value);
        Cookie cookie = new Cookie(TOKEN, value);
        cookie.setMaxAge(tokenMaxAge);
        return cookie;
    }

    private Cookie removeTokenCookie() {
        log.debug("Cookie removing");
        Cookie cookie = new Cookie(TOKEN, "");
        cookie.setMaxAge(0);
        return cookie;
    }

    private Map<String, String> getSignupErrors(SignupForm model) {
        log.debug("Validating form data: {}", model);
        Map<String, String> errors = new HashMap<>();
        String emptyValueMsg = "must be filled";
        if (isEmpty(model.getEmail())) {
            errors.put("email", emptyValueMsg);
        } else {
            if (!Pattern.matches("\\w+@\\w{2,}\\.[a-z]{2,4}", model.getEmail().toLowerCase())) {
                errors.merge("email", "invalid e-mail format", (s, s2) -> s.concat(", ").concat(s2));
            }
        }
        if (isEmpty(model.getLogin())) {
            errors.put("login", emptyValueMsg);
        } else {
            if (accountService.getByLogin(model.getLogin()) != null) {
                errors.merge("login", "Login is already in use", (s, s2) -> s.concat(", ").concat(s2));
            }
        }
        if (isEmpty(model.getPassword())) {
            errors.put("password", emptyValueMsg);
        }
        if (isEmpty(model.getName())) {
            errors.put("name", emptyValueMsg);
        }
        try {
            Sex.valueOf(model.getSex());
        } catch (RuntimeException e) {
            errors.put("sex", "Invalid value");
        }
        log.debug("Found errors: {}", errors);
        return errors;
    }
}
