<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="header.jsp"/>
<div class="col-md-6 col-md-offset-2">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12"><h4 class="text-center">${user.name}</h4></div>
            </div>
            <div class="row">
                <div class="col-md-4">Sex:</div>
                <div class="col-md-7 col-md-offset-1">${user.sex}</div>
            </div>
            <c:if test="${not empty user.birthDate}">
                <div class="row">
                    <div class="col-md-4">Birth date:</div>
                    <div class="col-md-7 col-md-offset-1"><fmt:formatDate value="${user.birthDate}"
                                                                          pattern="dd.MM.yyyy"/></div>
                </div>
            </c:if>
            <c:if test="${contact.size() > 0}">
                <h5>Contacts</h5>
                <c:forEach var="contact" items="${contact}">
                    <div class="row">
                        <div class="col-md-4">${contact.type}</div>
                        <div class="col-md-7 col-md-offset-1">${contact.value}</div>
                    </div>
                </c:forEach>
            </c:if>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="${photoUrl}"/>
            </div>
        </div>
    </div>
    <div class="col-row">
        <h5>Wall</h5>
    </div>

</div>
<jsp:include page="footer.jsp"/>