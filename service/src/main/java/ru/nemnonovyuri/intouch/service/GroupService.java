package ru.nemnonovyuri.intouch.service;

import ru.nemnonovyuri.intouch.dao.AccountDao;
import ru.nemnonovyuri.intouch.dao.GroupDao;
import ru.nemnonovyuri.intouch.dto.AccountTO;
import ru.nemnonovyuri.intouch.dto.GroupTO;
import ru.nemnonovyuri.intouch.exceptions.AccountNotFoundException;
import ru.nemnonovyuri.intouch.exceptions.GroupNotFoundException;
import ru.nemnonovyuri.intouch.model.Account;
import ru.nemnonovyuri.intouch.model.Group;
import ru.nemnonovyuri.intouch.model.MemberRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupService extends BaseService {
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private AccountDao accountDao;

    private static final Logger log = LoggerFactory.getLogger(GroupService.class);

    public GroupService() {
    }

    /* Group */
    public List<GroupTO> getAll() {
        log.debug("getAll()");
        return groupDao.getAll().stream().map(GroupTO::new).collect(Collectors.toList());
    }

    public GroupTO getGroup(int id) {
        log.debug("getGroup({})", id);
        Group group = getGroupById(id);
        return new GroupTO(group);
    }

    public List<GroupTO> getByTitle(String title) {
        log.debug("getByTitle({})", title);
        return groupDao.getByTitle(title).stream().map(GroupTO::new).collect(Collectors.toList());
    }

    public List<GroupTO> getByTitlePaginated(String title, int pageNum, int pageSize) {
        log.debug("getByTitlePaginated({}, {}, {})", title, pageNum, pageSize);
        return groupDao.getByTitle(title, pageSize, (pageNum - 1) * pageSize).stream().map(GroupTO::new).collect(Collectors.toList());
    }

    public Integer getSearchByTitleResultCount(String title) {
        log.debug("getSearchByTitleResultCount({})", title);
        return groupDao.getQueryByTitleResultCount(title);
    }

    @Transactional
    public GroupTO createGroup(GroupTO to, AccountTO accountTO) {
        log.debug("createGroup({}, {})", to, accountTO);
        Group group = new Group(to.getName(), to.getTitle(), to.getDescription());
        Account account = getAccountById(accountTO.getId());
        group = groupDao.save(group);
        groupDao.addMember(group, account, MemberRole.ADMIN);
        return new GroupTO(group);
    }

    @Transactional
    public void updateGroup(GroupTO to) {
        log.debug("updateGroup({})", to);
        checkIdNotNull(to);
        Group group = getGroupById(to.getId());
        group.setTitle(to.getTitle());
        group.setName(to.getName());
        group.setDescription(to.getDescription());
        group.setPhoto(to.getPhoto());
        groupDao.save(group);
    }

    public MemberRole getMemberRole(GroupTO groupTO, AccountTO accountTO) {
        log.debug("getMemberRole({}, {})", groupTO, accountTO);
        return groupDao.getMemberRole(getGroupById(groupTO.getId()), getAccountById(accountTO.getId()));
    }

    public boolean hasUpdatePermission(GroupTO groupTO, AccountTO accountTO) {
        log.debug("hasUpdatePermission({}, {})", groupTO, accountTO);
        MemberRole role = getMemberRole(groupTO, accountTO);
        return role != null && (role == MemberRole.ADMIN || role == MemberRole.MODERATOR);
    }

    private Account getAccountById(int id) {
        log.debug("getAccountById({})", id);
        Account acc = accountDao.get(id);
        if (acc == null) {
            throw new AccountNotFoundException(id);
        }
        return acc;
    }

    private Group getGroupById(int id) {
        log.debug("getGroupById({})", id);
        Group group = groupDao.get(id);
        if (group == null) {
            throw new GroupNotFoundException(id);
        }
        return group;
    }
}
