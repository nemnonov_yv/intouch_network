package ru.nemnonovyuri.intouch.dto;

import ru.nemnonovyuri.intouch.model.FriendshipRequest;
import ru.nemnonovyuri.intouch.model.RequestStatus;

public class FriendshipRequestTO extends IdentityTO {
    private Integer senderId;
    private Integer recipientId;
    private RequestStatus status;

    public FriendshipRequestTO() {
    }

    public FriendshipRequestTO(FriendshipRequest request) {
        this(request.getId(), request.getSender().getId(), request.getRecipient().getId(), request.getStatus());
    }

    public FriendshipRequestTO(Integer id, Integer senderId, Integer recipientId, RequestStatus status) {
        super(id);
        this.senderId = senderId;
        this.recipientId = recipientId;
        this.status = status;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public Integer getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(Integer recipientId) {
        this.recipientId = recipientId;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }
}
