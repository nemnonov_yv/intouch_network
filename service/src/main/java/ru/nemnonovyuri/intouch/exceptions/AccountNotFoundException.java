package ru.nemnonovyuri.intouch.exceptions;

public class AccountNotFoundException extends ServiceException {
    public AccountNotFoundException() {
        super();
    }

    public AccountNotFoundException(int id) {
        this("id=" + id);
    }

    public AccountNotFoundException(String message) {
        super(message);
    }

    public AccountNotFoundException(Throwable cause) {
        super(cause);
    }

    public AccountNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
