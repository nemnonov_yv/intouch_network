<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--<%@page import="ContactType" %>--%>
<form method="post" action="/profile/update/contacts" id="contacts-form">
    <form:form method="post" action="/profile/update/contacts" id="contacts-form" modelAttribute="contacts">
    <c:forEach items="${contacts}" var="contact" varStatus="status">
    <div class="row">
        <input type="hidden" name="id" value="${contact.id}">
        <input type="hidden" name="delete" value="0">

        <div class="col-md-4">
            <div class="form-group">
                <select class="form-control" name="type">
                    <c:forEach items="${types}" var="type">
                        <option value="${type.numeric}"${type == contact.type ? ' selected' : ''}>${type}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <input type="text" name="value" value="${contact.value}" class="form-control">
            </div>
        </div>
        <div class="col-md-1">
            <div class="btn btn-danger remove-contact"><i class="glyphicon glyphicon-trash"></i></div>
        </div>
    </div>
    </c:forEach>
    <div class="row">
        <input type="hidden" name="id" value="null">
        <input type="hidden" name="delete" value="0">

        <div class="col-md-4">
            <div class="form-group">
                <select class="form-control" name="type">
                    <c:forEach items="${types}" var="type">
                        <option value="${type.numeric}">${type}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <input type="text" name="value" class="form-control" autocomplete="off">
            </div>
        </div>
    </div>
    <p>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".confirm-dlg">Save</button>
        <a href='#' class="btn btn-default" id="add-row"><i class="glyphicon glyphicon-plus"></i> Add more</a>
    </p>
    </form:form>
    <div class="modal fade confirm-dlg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirmation required</h4>
                </div>
                <div class="modal-body">
                    Are you sure to change contact information?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="confirm-dlg-save-btn">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            ContactFormHandler();
        });
    </script>
