package ru.nemnonovyuri.intouch.service;

import ru.nemnonovyuri.intouch.dao.AccountDao;
import ru.nemnonovyuri.intouch.dao.FriendshipRequestDao;
import ru.nemnonovyuri.intouch.dao.GroupDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nemnonovyuri.intouch.PasswordUtils;
import ru.nemnonovyuri.intouch.dto.*;
import ru.nemnonovyuri.intouch.exceptions.*;
import ru.nemnonovyuri.intouch.model.*;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.nemnonovyuri.intouch.util.StringUtils.isEmpty;

@Service
public class AccountService extends BaseService {
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private FriendshipRequestDao requestDao;
    @Autowired
    private SecurityService securityService;

    private static final Logger log = LoggerFactory.getLogger(AccountService.class);

    public AccountService() {
    }

    /* Account */

    public List<AccountTO> getAll() {
        log.debug("getAll()");
        return accountDao.getAll().stream().map(AccountTO::new).collect(Collectors.toList());
    }

    public AccountTO getAccount(int id) {
        log.debug("getAccount({})", id);
        return new AccountTO(getAccountById(id));
    }

    @Transactional
    public AccountInfoTO getAccountInfo(int id) {
        Account account = getAccountById(id);
        AccountInfoTO fullTO = new AccountInfoTO(account);
        fullTO.setContact(account.getContacts().stream().map(ContactTO::new).collect(Collectors.toSet()));
        return fullTO;
    }

    @Transactional
    public AccountTO registerNewAccount(AccountTO to, String email) {
        log.debug("registerNewAccount({}, {})", to, email);
        validateAccountTO(to);
        if (getByLogin(to.getLogin()) != null) {
            throw new AccountExistsRegistrationException(to.toString());
        }
        if (isEmpty(to.getPassword())) {
            throw new EmptyPasswordRegistrationException(to.toString());
        }
        Account account = new Account();
        account.setLogin(to.getLogin());
        account.setName(to.getName());
        account.setBirthDate(to.getBirthDate());
        account.setSex(to.getSex());
        account.setPasswordHash(PasswordUtils.generateHash(to.getPassword()));
        account.getContacts().add(new ContactInfo(account, ContactType.PRIMARY_EMAIL, email));
        AccountTO registeredAccount = new AccountTO(accountDao.save(account));
        log.info("account registered: {}", registeredAccount);
        return registeredAccount;
    }

    public Account getByLogin(String login) {
        log.debug("getByLogin({})", login);
        return accountDao.getByLogin(login);   //TODO test
    }

    public List<AccountTO> getByName(String name) {
        log.debug("getByName({})", name);
        return accountDao.getByName(name).stream().map(AccountTO::new).collect(Collectors.toList());
    }

    public List<AccountTO> getByNamePaginated(String name, int pageNum, int pageSize) {
        log.debug("getByNamePaginated({}, {}, {})", name, pageNum, pageSize);
        return accountDao.getByName(name, pageSize, (pageNum - 1) * pageSize).stream().map(AccountTO::new).collect(Collectors.toList());
    }

    public Integer getSearchByNameResultCount(String name) {
        log.debug("getSearchByNameResultCount({})", name);
        return accountDao.getQueryByNameResultCount(name);
    }

    @Transactional
    public void updateAccount(AccountTO to) {
        log.debug("updateAccount({})", to);
        checkIdNotNull(to);
        validateAccountTO(to);
        Account account = getAccountById(to.getId());
        account.setName(to.getName());
        account.setBirthDate(to.getBirthDate());
        account.setSex(to.getSex());
        account.setPhoto(to.getPhoto());
        accountDao.save(account);
    }

    public AccountTO getIfCredentialsCorrect(AccountTO to) {
        log.debug("getIfCredentialsCorrect({})", to);
        Account account = getByLogin(to.getLogin());
        return account != null && PasswordUtils.isPasswordCorrect(account, to.getPassword()) ? new AccountTO(account) : null;
    }

    @Transactional
    public void changePassword(PasswordChangingTO to) {
        log.debug("changePassword({})", to);
        if (isEmpty(to.getNewPassword()) || isEmpty(to.getOldPassword())) {
            throw new InvalidRequestException("Password must not be empty");
        }
        Account account = getAccountById(to.getAccountId());
        if (!PasswordUtils.isPasswordCorrect(account, to.getOldPassword())) {
            throw new AccessViolationException("Old password incorrect");
        }
        account.setPasswordHash(PasswordUtils.generateHash(to.getNewPassword()));
        accountDao.save(account);
    }

    @Transactional
    public void deleteAccount(AccountTO to) {
        log.debug("deleteAccount({})", to);
        checkIdNotNull(to);
        Account acc = getAccountById(to.getId());
        if (!PasswordUtils.isPasswordCorrect(acc, to.getPassword())) {
            throw new AccessViolationException(to.toString() + " - wrong password");
        }
        accountDao.delete(acc);
    }

    /* Contact info */

    @Transactional
    public AccountContactsTO getContactInfo(AccountTO to) {
        log.debug("getContactInfo({})", to);
        return getContactInfoByType(to, null);
    }

    @Transactional
    public AccountContactsTO getContactInfoByType(AccountTO to, ContactType type) {
        log.debug("getContactInfoByType({}, {})", to, type);
        checkIdNotNull(to);
        AccountContactsTO contactsTO = new AccountContactsTO(to.getId());
        Account account = getAccountById(to.getId());
        account.getContacts()
                .stream()
                .filter(contact -> type == null || type == contact.getType())
                .forEach(contact -> contactsTO.addContact(new ContactTO(contact)));
        return contactsTO;
    }

    @Transactional
    public void updateContacts(AccountContactsTO contactsTO) {
        log.debug("updateContacts({})", contactsTO);
        Account acc = getAccountById(contactsTO.getAccountId());
        Set<ContactInfo> accContacts = acc.getContacts();
        //updating existing contacts
        for (ContactInfo contact : accContacts) {
            ContactTO contactTO = new ContactTO(contact);
            Iterator<ContactTO> it = contactsTO.getContacts().iterator();
            while (it.hasNext()) {
                ContactTO to = it.next();
                if (to.getId() != null && to.getId().equals(contactTO.getId())) {
                    contact.setType(to.getType());
                    contact.setValue(to.getValue());
                    it.remove();
                    break;
                }
            }
        }
        accContacts.addAll(contactsTO.getContacts().stream()
                .filter(contactTO -> contactTO.getId() == null)
                .map(contactTO -> new ContactInfo(acc, contactTO.getType(), contactTO.getValue()))
                .collect(Collectors.toList()));
        accountDao.save(acc);
    }

    @Transactional
    public void deleteContacts(AccountContactsTO contactsTO) {
        log.debug("deleteContacts({})", contactsTO);
        Account acc = getAccountById(contactsTO.getAccountId());
        Iterator<ContactInfo> contactInfoIterator = acc.getContacts().iterator();
        while (contactInfoIterator.hasNext()) {
            ContactTO contactTO = new ContactTO(contactInfoIterator.next());
            if (contactsTO.getContacts().contains(contactTO)) {
                contactInfoIterator.remove();
            }
        }
        accountDao.save(acc);
    }

    /* Relations */

    @Transactional
    public void sendFriendshipRequest(AccountTO senderTO, AccountTO recipientTO) {
        checkIdNotNull(senderTO, recipientTO);

        Account sender = getAccountById(senderTO.getId());
        Account recipient = getAccountById(recipientTO.getId());

        //forbid request sending if accounts are already have relation
        if (accountDao.hasFriendship(sender, recipient)) {
            throw new ServiceException("Relation already established");
        }
        //request resending not allowed
        if (requestDao.getByAccounts(sender, recipient) != null) {
            throw new ServiceException("Request already send");
        }
        //if request was already received from recipient - accept it
        FriendshipRequest recipientRequest = requestDao.getByAccounts(recipient, sender);
        if (recipientRequest != null) {
            acceptFriendshipRequest(recipientRequest);
        } else {
            requestDao.save(new FriendshipRequest(sender, recipient));
        }
    }

    @Transactional
    public void acceptFriendshipRequest(FriendshipRequestTO to) {
        checkIdNotNull(to);
        Account sender = getAccountById(to.getSenderId());
        Account recipient = getAccountById(to.getRecipientId());
        FriendshipRequest request = requestDao.get(to.getId());
        //try to avoid fake requests
        if (request != null && request.getSender().equals(sender) && request.getRecipient().equals(recipient)) {
            acceptFriendshipRequest(request);
        }
    }

    @Transactional
    private void acceptFriendshipRequest(FriendshipRequest request) {
        Account sender = request.getSender();
        Account recipient = request.getRecipient();
        setFriendship(sender, recipient);
        requestDao.delete(request);
    }

    @Transactional
    private void setFriendship(Account acc1, Account acc2) {
        acc1.getFriends().add(acc2);
        acc2.getFriends().add(acc1);
        accountDao.save(acc1);
        accountDao.save(acc2);
    }

    @Transactional
    public List<FriendshipRequestTO> getPendingIncomeFriendshipRequests(AccountTO to) {
        checkIdNotNull(to);
        Account acc = getAccountById(to.getId());
        return acc.getIncomeRequests()
                .stream()
                .filter(request -> request.getStatus() == RequestStatus.PENDING)
                .map(FriendshipRequestTO::new).collect(Collectors.toList());
    }

    @Transactional
    public List<FriendshipRequestTO> getPendingOutcomeFriendshipRequests(AccountTO to) {
        checkIdNotNull(to);
        Account acc = getAccountById(to.getId());
        return acc.getOutcomeRequests()
                .stream()
                .filter(request -> request.getStatus() == RequestStatus.PENDING)
                .map(FriendshipRequestTO::new).collect(Collectors.toList());
    }

    @Transactional
    public void declineFriendshipRequest(FriendshipRequestTO to) {
        checkIdNotNull(to);
        FriendshipRequest request = requestDao.get(to.getId());
        Account recipient = getAccountById(to.getRecipientId());
        if (request != null && request.getRecipient().equals(recipient)) {
            request.setStatus(RequestStatus.DECLINED);
            requestDao.save(request);
        }
    }

    @Transactional
    public void revokeFriendshipRequest(FriendshipRequestTO to) {
        checkIdNotNull(to);
        FriendshipRequest request = requestDao.get(to.getId());
        Account sender = getAccountById(to.getSenderId());
        if (request != null && request.getSender().equals(sender)) {
            requestDao.delete(request);
        }
    }

    @Transactional
    public void deleteFriend(AccountTO accountTO, AccountTO friendTO) {
        checkIdNotNull(accountTO, friendTO);
        Account acc = getAccountById(accountTO.getId());
        acc.getFriends().remove(getAccountById(friendTO.getId()));
        accountDao.save(acc);
    }

    @Transactional
    public List<AccountTO> getFriends(AccountTO to) {
        checkIdNotNull(to);
        return getAccountById(to.getId()).getFriends()
                .stream()
                .map(AccountTO::new).collect(Collectors.toList());
    }

    /* private utils */

    private Account getAccountById(int id) {
        Account account = accountDao.get(id);
        if (account == null) {
            throw new AccountNotFoundException(id);
        }
        return account;
    }

    /**
     * validating account transfer object
     */
    private void validateAccountTO(AccountTO to) {
        if (isEmpty(to.getLogin())) {
            throw new InvalidRequestException(to.toString());
        }
        if (to.getSex() == null) {
            throw new InvalidRequestException(to.toString());
        }
    }
}
