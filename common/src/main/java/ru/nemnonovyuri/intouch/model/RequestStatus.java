package ru.nemnonovyuri.intouch.model;

public enum RequestStatus {
    PENDING(0), ACCEPTED(1), DECLINED(2);

    private int numeric;

    RequestStatus(int i) {
        numeric = i;
    }

    public static RequestStatus valueOf(int numeric) {
        for (RequestStatus role : RequestStatus.values()) {
            if (role.getNumeric() == numeric) {
                return role;
            }
        }
        throw new RuntimeException("Invalid request status numeric presentation");
    }

    public int getNumeric() {
        return numeric;
    }
}
