package ru.nemnonovyuri.intouch.exceptions;

public class AccessViolationException extends ServiceException {
    public AccessViolationException() {
        super();
    }

    public AccessViolationException(String message) {
        super(message);
    }

    public AccessViolationException(Throwable cause) {
        super(cause);
    }

    public AccessViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
