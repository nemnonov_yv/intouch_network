<form class="navbar-form navbar-right" action="/search">
    <div class="form-group">
        <input type="text" name="name" class="form-control" placeholder="Search" id="header-searchbox">
    </div>
    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
</form>