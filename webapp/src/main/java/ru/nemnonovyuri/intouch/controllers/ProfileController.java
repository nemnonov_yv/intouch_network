package ru.nemnonovyuri.intouch.controllers;

import ru.nemnonovyuri.intouch.dto.*;
import ru.nemnonovyuri.intouch.formmodels.GeneralInfoForm;
import ru.nemnonovyuri.intouch.helpers.AccountHelper;
import ru.nemnonovyuri.intouch.helpers.ImageUploads;
import ru.nemnonovyuri.intouch.helpers.XmlHelper;
import ru.nemnonovyuri.intouch.model.ContactType;
import ru.nemnonovyuri.intouch.model.Sex;
import ru.nemnonovyuri.intouch.service.AccountService;
import ru.nemnonovyuri.intouch.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.regex.Pattern;

import static ru.nemnonovyuri.intouch.helpers.XmlHelper.getFromXml;
import static ru.nemnonovyuri.intouch.util.StringUtils.isEmpty;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping("/profile")
public class ProfileController extends BaseController {
    private static Logger log = LoggerFactory.getLogger(ProfileController.class);

    private static String REDIRECT_TO = "redirect:/profile/edit";

    @Autowired
    AccountService accountService;

    @RequestMapping("")
    public ModelAndView showMyProfile(@SessionAttribute AccountTO account) {
        log.debug("showMyProfile({})", account);
        ModelAndView view = getAccountModelView("my_profile", account);
        setTitle(view, "My profile");
        return view;
    }

    @RequestMapping("/edit")
    public ModelAndView editProfile(@SessionAttribute AccountTO account) {
        log.debug("editProfile({})", account);
        ModelAndView view = getAccountModelView("profile_edit", account);
        setTitle(view, "My profile - edit");
        return view;
    }

    @RequestMapping(value = "/download-xml", produces = "application/octet-stream")
    public void download(HttpServletResponse response, @SessionAttribute AccountTO account) throws ServletException, IOException {
        response.setContentType("application/xml");
        response.addHeader("Content-Disposition", "attachment; filename=" + account.getLogin() + ".xml");
        String xml = XmlHelper.getXml(accountService.getAccountInfo(account.getId()));
        response.getOutputStream().write(xml.getBytes());
    }

    @RequestMapping(value = "/update/general", method = POST)
    public String updateGeneralInfo(GeneralInfoForm model, RedirectAttributes redirectAttr, HttpServletRequest request) {
        log.debug("updateGeneralInfo({})", model);
        int currentTab = 0;
        AccountTO account = getAccountFromSession(request);
        AccountTO to = new AccountTO();
        to.setId(account.getId());
        to.setLogin(account.getLogin());
        to.setName(model.getName());
        if (!isEmpty(model.getBirthDate())) {
            try {
                to.setBirthDate(DateUtils.parseString(model.getBirthDate()));
            } catch (ParseException e) {
                log.warn("Birth date user input conversion error: {}", model.getBirthDate());
                setUpdateStatusFailed(redirectAttr, "Invalid birth date format", currentTab);
                return REDIRECT_TO;
            }
        }
        to.setSex(Sex.valueOf(model.getSex()));
        String uploaded = ImageUploads.upload(model.getPhoto(), account, ImageUploads.Type.USER_PHOTO);
        String oldPhotoFilename = account.getPhoto();
        to.setPhoto(uploaded != null ? uploaded : oldPhotoFilename);
        accountService.updateAccount(to);
        request.getSession().setAttribute("account", to);
        if (uploaded != null) {
            try {
                ImageUploads.delete(account, oldPhotoFilename, ImageUploads.Type.USER_PHOTO);
            } catch (IOException e) {
                log.warn("Removing user photo error: {}, {}", oldPhotoFilename, e.getMessage());
            }
        }
        setUpdateStatusSuccess(redirectAttr, currentTab);
        return REDIRECT_TO;
    }

    //TODO list
    @RequestMapping(value = "/update/contacts", method = POST)
    public String updateContactInfo(HttpServletRequest request, RedirectAttributes redirectAttr) {
        int currentTab = 1;
        String[] ids = request.getParameterValues("id");
        String[] deleted = request.getParameterValues("delete");
        String[] types = request.getParameterValues("type");
        String[] values = request.getParameterValues("value");
        log.debug("Updating contact info request: id {}, delete {}, types {}, values {}", ids, deleted, types, values);
        int size = ids.length;
        if (deleted.length != size || types.length != size || values.length != size) {
            setUpdateStatusFailed(redirectAttr, "Invalid request", currentTab);
            log.warn("Invalid request");
            return REDIRECT_TO;
        }
        AccountTO account = getAccountFromSession(request);
        Integer accountId = account.getId();
        AccountContactsTO updateContacts = new AccountContactsTO(accountId);
        AccountContactsTO deleteContacts = new AccountContactsTO(accountId);
        for (int i = 0; i < size; i++) {
            Integer id = ids[i].equals("null") || isEmpty(ids[i]) ? null : Integer.valueOf(ids[i]);
            boolean delete = deleted[i].equals("1");
            ContactType type = ContactType.valueOf(Integer.valueOf(types[i]));
            String value = values[i];
            ContactTO contactTO = new ContactTO(id, type, value);
            if (id != null && delete) {
                deleteContacts.addContact(contactTO);
                continue;
            }
            if (!isEmpty(value)) {
                if (!isValidContact(type, value)) {
                    setUpdateStatusFailed(redirectAttr, type + " doesn't match pattern +123(123)1234567", currentTab);
                    return REDIRECT_TO;
                }
                updateContacts.addContact(contactTO);
            }
        }
        accountService.updateContacts(updateContacts);
        accountService.deleteContacts(deleteContacts);
        setUpdateStatusSuccess(redirectAttr, currentTab);
        return REDIRECT_TO;
    }

    @RequestMapping(value = "/update/password", method = POST)
    public String updatePassword(String oldPassword, String newPassword, HttpServletRequest request, RedirectAttributes redirectAttr) {
        int currentTab = 2;
        if (isEmpty(oldPassword) || isEmpty(newPassword)) {
            setUpdateStatusFailed(redirectAttr, "Password must not be empty", currentTab);
            return REDIRECT_TO;
        }
        AccountTO account = getAccountFromSession(request);
        AccountTO to = new AccountTO();
        to.setLogin(account.getLogin());
        to.setPassword(oldPassword);
        if (accountService.getIfCredentialsCorrect(to) != null) {
            PasswordChangingTO passTO = new PasswordChangingTO(account.getId(), oldPassword, newPassword);
            accountService.changePassword(passTO);
            setUpdateStatusSuccess(redirectAttr, currentTab);
        } else {
            setUpdateStatusFailed(redirectAttr, "Old password is incorrect", currentTab);
        }
        return REDIRECT_TO;
    }

    @RequestMapping(value = "/update/from-xml", method = POST)
    public ModelAndView updateFromXml(MultipartFile xmlfile, @SessionAttribute AccountTO account) {
        AccountInfoTO infoTO = getFromXml(xmlfile);
        if (infoTO.getLogin() == null) {
            log.info("updateFromXml() - failed");
        }
        ModelAndView view = new ModelAndView("profile_edit");
        AccountTO xmlAccTO = new AccountTO(account.getId(), infoTO.getLogin(), infoTO.getName(), infoTO.getBirthDate(), infoTO.getSex());
        view.addObject("account", xmlAccTO);
        view.addObject("contacts", infoTO.getContact());
        view.addObject("types", ContactType.values());
        view.addObject("photoUrl", AccountHelper.getAccountPhotoUrl(account));
        return view;
    }

    private ModelAndView getAccountModelView(String viewName, AccountTO accountTO) {
        log.debug("getAccountModelView({}, {})", viewName, accountTO);
        ModelAndView view = new ModelAndView(viewName);
        view.addObject("account", accountTO);
        view.addObject("contacts", accountService.getContactInfo(accountTO).getContacts());
        view.addObject("types", ContactType.values());
        view.addObject("photoUrl", AccountHelper.getAccountPhotoUrl(accountTO));
        return view;
    }

    private void setUpdateStatusFailed(RedirectAttributes redirectAttr, String error, int activeTab) {
        log.debug("Update failed with message: {}, active tab: {}", error, activeTab);
        redirectAttr.addFlashAttribute("status", "failed");
        redirectAttr.addFlashAttribute("error", error);
        redirectAttr.addFlashAttribute("activeTab", activeTab);
    }

    private void setUpdateStatusSuccess(RedirectAttributes redirectAttr, int activeTab) {
        redirectAttr.addFlashAttribute("status", "OK");
        redirectAttr.addFlashAttribute("activeTab", activeTab);
    }

    private boolean isValidContact(ContactType type, String value) {
        if (type == ContactType.PERSONAL_PHONE || type == ContactType.WORK_PHONE) {
            return Pattern.matches("\\+(\\d{1,3})\\((\\d{3})\\)(\\d{5,7})", value);
        }
        return true;
    }
}
