package ru.nemnonovyuri.intouch.dao;

import ru.nemnonovyuri.intouch.model.Account;
import ru.nemnonovyuri.intouch.model.AccountStatus;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AccountDao extends JpaDao<Account> {
    @Override
    @Transactional
    public void delete(Account acc) {
        Account deleted = get(acc.getId());
        if (deleted != null) {
            deleted.setStatus(AccountStatus.DELETED);
            save(deleted);
        }
    }

    public List<Account> getAll() {
        return getAll(0, 0);
    }

    public List<Account> getAll(int limit, int offset) {
        CriteriaQuery<Account> selectQuery = getSelectQuery("name");
        TypedQuery<Account> query = entityManager.createQuery(selectQuery);
        return limit == 0 ? query.getResultList()
                : query.setFirstResult(offset).setMaxResults(limit).getResultList();
    }

    public Integer getQueryAllResultCount() {
        CriteriaQuery<Long> countQuery = getCountQuery();
        return entityManager.createQuery(countQuery).getSingleResult().intValue();
    }

    public Account getByLogin(String login) {
        try {
            CriteriaQuery<Account> selectQuery = getSelectQuery(null);
            selectQuery.where(entityManager.getCriteriaBuilder().equal(getRoot(selectQuery).get("login"), login));
            return entityManager.createQuery(selectQuery).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Account> getByName(String name) {
        return getByName(name, 0, 0);
    }

    public List<Account> getByName(String name, int limit, int offset) {
        try {
            CriteriaQuery<Account> selectQuery = getSelectQuery("name");
            selectQuery.where(entityManager.getCriteriaBuilder().like(
                    getRoot(selectQuery).get("name"), "%" + name + "%"));
            TypedQuery<Account> query = entityManager.createQuery(selectQuery);
            return limit == 0 ? query.getResultList()
                    : query.setFirstResult(offset).setMaxResults(limit).getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

    public Integer getQueryByNameResultCount(String name) {
        CriteriaQuery<Long> countQuery = getCountQuery();
        countQuery.where(entityManager.getCriteriaBuilder().like(getRoot(countQuery).get("name"), "%" + name + "%"));
        return entityManager.createQuery(countQuery).getSingleResult().intValue();
    }

    public boolean hasFriendship(Account acc1, Account acc2) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> selectQuery = getSelectQuery(null);
        Root<Account> root = getRoot(selectQuery);
        selectQuery.where(cb.and(
                cb.equal(root.get("account"), acc1),
                cb.in(root.get("friend")).value(acc2)));
        return entityManager.createQuery(selectQuery).getSingleResult() != null;
    }
}
