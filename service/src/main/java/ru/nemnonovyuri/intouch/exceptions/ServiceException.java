package ru.nemnonovyuri.intouch.exceptions;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceException extends RuntimeException {
    private static final Logger log = LoggerFactory.getLogger("ru.nemnonovyuri.intouch.exceptions");

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
        log.warn(message);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
