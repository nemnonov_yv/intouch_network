package ru.nemnonovyuri.intouch.formmodels;

import org.springframework.web.multipart.MultipartFile;

public class GeneralInfoForm {
    private String name;
    private String birthDate;
    private Integer sex;
    private MultipartFile photo;

    public GeneralInfoForm() {
    }

    public GeneralInfoForm(String name, String birthDate, Integer sex) {
        this.name = name;
        this.birthDate = birthDate;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "GeneralInfoForm{" +
                "name='" + name + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", sex=" + sex +
                ", photo=" + photo.getName() +
                '}';
    }
}
