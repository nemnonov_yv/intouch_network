<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:include page="header.jsp"/>
<div class="col-md-6 col-md-offset-2">
    <form method="post" action="/groups/${action}" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-6">
                <input type="hidden" name="id" value="${group.id}">

                <div class="form-group">
                    <label for="name">Unique name</label>
                    <input type="text" name="name" class="form-control" id="name" value="${group.name}" required
                           autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" id="title" value="${group.title}" required
                           autocomplete="off">
                </div>
            </div>
            <div class="col-md-6">
                <label for="photo">Photo</label>

                <div class="thumbnail">
                    <img src="${photoUrl}"/>
                </div>
                <div class="text-center"><input type="file" id="photo" name="photo"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" class="form-control" id="description"
                              required>${group.description}</textarea>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-default">Save</button>
    </form>
</div>
<jsp:include page="footer.jsp"/>