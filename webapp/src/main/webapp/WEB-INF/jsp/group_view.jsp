<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:include page="header.jsp"/>
<div class="col-md-6 col-md-offset-2">
    <div class="row">
        <div class="col-md-8">
            <h4>${group.title}</h4>

            <p>${group.description}</p>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="${photoUrl}"/>
            </div>
        </div>
    </div>
    <div class="row">
        <h4>Messages</h4>

        <p>Will be soon, stay tuned :)</p>
    </div>
</div>
<jsp:include page="footer.jsp"/>