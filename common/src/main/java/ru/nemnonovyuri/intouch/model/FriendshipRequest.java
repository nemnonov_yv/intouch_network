package ru.nemnonovyuri.intouch.model;

import ru.nemnonovyuri.intouch.model.converters.RequestStatusJpaConverter;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "relation_requests")
public class FriendshipRequest extends AbstractIdentity {
    @ManyToOne
    private Account sender;
    @ManyToOne
    private Account recipient;
    @Convert(converter = RequestStatusJpaConverter.class)
    private RequestStatus status;

    public FriendshipRequest() {
    }

    public FriendshipRequest(Account sender, Account recipient) {
        this(sender, recipient, RequestStatus.PENDING);
    }

    public FriendshipRequest(Account sender, Account recipient, RequestStatus status) {
        this.sender = sender;
        this.recipient = recipient;
        this.status = status;
    }

    public Account getSender() {
        return sender;
    }

    public void setSender(Account sender) {
        this.sender = sender;
    }

    public Account getRecipient() {
        return recipient;
    }

    public void setRecipient(Account recipient) {
        this.recipient = recipient;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FriendshipRequest that = (FriendshipRequest) o;

        return (getSender().equals(that.getSender()) && getRecipient().equals(that.getRecipient()));
    }

    @Override
    public int hashCode() {
        int result = getSender().hashCode();
        result = 31 * result + getRecipient().hashCode();
        result = 31 * result + getStatus().hashCode();
        return result;
    }
}
