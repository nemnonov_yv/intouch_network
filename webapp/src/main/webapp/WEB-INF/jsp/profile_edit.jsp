<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="header.jsp"/>
<div class="col-md-8 col-md-offset-2">
    <h3>
        <a href="/profile" class="btn btn-sm btn-default" title="back to my page">
            <i class="glyphicon glyphicon-arrow-left"></i>
        </a> Profile edit
    </h3>
    <c:if test="${status eq 'OK'}">
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <strong>Changes was successfully saved</strong>
        </div>
    </c:if>
    <c:if test="${status eq 'failed'}">
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <strong>Failure</strong>

            <p>${error}</p>
        </div>
    </c:if>
    <div id="tabs">
        <ul>
            <li><a href="#tab1">General</a></li>
            <li><a href="#tab2">Contacts</a></li>
            <li><a href="#tab3">Password</a></li>
        </ul>
        <div id="tab1">
            <jsp:include page="general_form.jsp"/>
        </div>
        <div id="tab2">
            <jsp:include page="contacts_form.jsp"/>
        </div>
        <div id="tab3">
            <jsp:include page="password_form.jsp"/>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Update from XML file</h3>
        </div>
        <div class="panel-body">
            <form method="post" action="/profile/update/from-xml" enctype="multipart/form-data" id="upload-xml">
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="file" class="form-control" id="xmlfile" name="xmlfile">
						<span class="input-group-btn">
							<button class="btn btn-default" type="submit">Upload</button>
						</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-3">
                        <a href="/profile/download-xml" class="btn btn-info" target="_blank">Download data as XML</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<jsp:include page="footer.jsp"/>
<script>
    $('#tabs').tabs(
            <c:if test="${not empty activeTab}">
            {active: ${activeTab}}
            </c:if>
    );
</script>