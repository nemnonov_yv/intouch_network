<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="ru.nemnonovyuri.intouch.model.Sex" %>
<%--<%@page import="Sex" %>--%>
<form method="post" action="/profile/update/general" enctype="multipart/form-data" id="general-info">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" id="name" value="${account.name}" required
                       autocomplete="off">
            </div>
            <div class="form-group">
                <label for="birthdate">Birth date</label>
                <input type="text" name="birthDate" class="form-control" id="birthdate"
                       value="<fmt:formatDate type="date" value="${account.birthDate}" pattern="dd.MM.yyyy"/>"
                       autocomplete="off"/>
            </div>
            <div class="form-group">
                <label for="sex">Sex</label>
                <select name="sex" id="sex" class="form-control">
                    <option value="${Sex.UNSPECIFIED.getNumeric()}"
                            <c:if test="${account.sex == Sex.UNSPECIFIED}">selected</c:if>>
                        ${Sex.UNSPECIFIED}
                    </option>
                    <option value="${Sex.MALE.getNumeric()}" <c:if test="${account.sex == Sex.MALE}">selected</c:if>>
                        ${Sex.MALE}
                    </option>
                    <option value="${Sex.FEMALE.getNumeric()}"
                            <c:if test="${account.sex == Sex.FEMALE}">selected</c:if>>
                        ${Sex.FEMALE}
                    </option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <label for="photo">Photo</label>

            <div class="thumbnail">
                <img src="${photoUrl}"/>
            </div>
            <div class="text-center"><input type="file" id="photo" name="photo"></div>
        </div>
    </div>
    <button type="submit" class="btn btn-default">Save</button>
</form>
<script>
    $(function () {
        $('#birthdate').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd.mm.yy',
            yearRange: '-90:-12',
            defaultDate: '-12y'
        });
    });
</script>