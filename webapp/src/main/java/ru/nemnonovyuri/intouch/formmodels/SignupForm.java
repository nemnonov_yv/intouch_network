package ru.nemnonovyuri.intouch.formmodels;

import static ru.nemnonovyuri.intouch.util.StringUtils.isEmpty;

public class SignupForm {
    private String email;
    private String name;
    private String login;
    private String password;
    private Integer sex;

    public SignupForm() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "SignupForm{" +
                "email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", login='" + login + '\'' +
                ", password=" + (isEmpty(password) ? "<empty>" : "<not empty>") +
                ", sex=" + sex +
                '}';
    }
}
