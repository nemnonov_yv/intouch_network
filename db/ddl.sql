CREATE TABLE accounts
(
  id        INT(11) UNSIGNED PRIMARY KEY NOT NULL
  COMMENT 'Account ID' AUTO_INCREMENT,
  login     VARCHAR(100)                 NOT NULL
  COMMENT 'User login',
  pass      VARCHAR(255)                 NOT NULL
  COMMENT 'Password hash',
  name      VARCHAR(255)                 NOT NULL
  COMMENT 'User name',
  birthdate DATE COMMENT 'Birth date',
  sex       INT(1) UNSIGNED              NOT NULL,
  status    INT(1) UNSIGNED DEFAULT '0'  NOT NULL
  COMMENT 'Account status',
  photo     VARCHAR(255)
);
CREATE UNIQUE INDEX login ON accounts (login);

CREATE TABLE contact_info
(
  id     INT(11) UNSIGNED PRIMARY KEY NOT NULL
  COMMENT 'ID' AUTO_INCREMENT,
  acc_id INT(11) UNSIGNED             NOT NULL
  COMMENT 'Account ID',
  type   TINYINT(4) UNSIGNED          NOT NULL
  COMMENT 'Contact type',
  value  VARCHAR(255)                 NOT NULL
  COMMENT 'Contact value',
  CONSTRAINT FK_contact_info_accounts_id FOREIGN KEY (acc_id) REFERENCES accounts (id)
);
CREATE INDEX FK_contact_info_accounts_id ON contact_info (acc_id);

CREATE TABLE group_join_requests
(
  ID       INT(11) UNSIGNED PRIMARY KEY NOT NULL
  COMMENT 'ID' AUTO_INCREMENT,
  group_id INT(11) UNSIGNED             NOT NULL
  COMMENT 'Group ID',
  acc_id   INT(11) UNSIGNED             NOT NULL
  COMMENT 'Account ID',
  status   TINYINT(4) UNSIGNED          NOT NULL
  COMMENT 'Request status',
  CONSTRAINT FK_group_join_requests_accounts_id FOREIGN KEY (acc_id) REFERENCES accounts (id),
  CONSTRAINT FK_group_join_requests_groups_id FOREIGN KEY (group_id) REFERENCES groups (id)
);
CREATE INDEX FK_group_join_requests_accounts_id ON group_join_requests (acc_id);
CREATE INDEX FK_group_join_requests_groups_id ON group_join_requests (group_id);

CREATE TABLE group_members
(
  group_id INT(11) UNSIGNED    NOT NULL
  COMMENT 'Group ID',
  acc_id   INT(11) UNSIGNED    NOT NULL
  COMMENT 'Account ID',
  role     TINYINT(4) UNSIGNED NOT NULL
  COMMENT 'User role',
  CONSTRAINT FK_group_members_accounts_id FOREIGN KEY (acc_id) REFERENCES accounts (id),
  CONSTRAINT FK_group_members_groups_id FOREIGN KEY (group_id) REFERENCES groups (id)
);
CREATE INDEX FK_group_members_accounts_id ON group_members (acc_id);
CREATE UNIQUE INDEX UK_group_members ON group_members (group_id, acc_id);

CREATE TABLE groups
(
  id          INT(11) UNSIGNED PRIMARY KEY NOT NULL
  COMMENT 'ID' AUTO_INCREMENT,
  name        VARCHAR(100)                 NOT NULL
  COMMENT 'Unique group name',
  title       VARCHAR(255)                 NOT NULL
  COMMENT 'Group title',
  description TEXT                         NOT NULL
  COMMENT 'Group description',
  photo       VARCHAR(255)
);
CREATE UNIQUE INDEX name ON groups (name);

CREATE TABLE relation_requests
(
  ID           INT(11) UNSIGNED PRIMARY KEY NOT NULL
  COMMENT 'ID' AUTO_INCREMENT,
  sender_id    INT(11) UNSIGNED             NOT NULL
  COMMENT 'Sender account ID',
  recipient_id INT(11) UNSIGNED             NOT NULL
  COMMENT 'Recipient account ID',
  status       TINYINT(4) UNSIGNED          NOT NULL
  COMMENT 'Request status',
  CONSTRAINT FK_relation_requests_recipient_account_id FOREIGN KEY (recipient_id) REFERENCES accounts (id),
  CONSTRAINT FK_relation_requests_sender_account_id FOREIGN KEY (sender_id) REFERENCES accounts (id)
);
CREATE INDEX FK_relation_requests_recipient_account_id ON relation_requests (recipient_id);
CREATE INDEX FK_relation_requests_sender_account_id ON relation_requests (sender_id);

CREATE TABLE relations
(
  acc1_id INT(11) UNSIGNED NOT NULL
  COMMENT 'First account ID',
  acc2_id INT(11) UNSIGNED NOT NULL
  COMMENT 'Second account ID',
  CONSTRAINT FK_relations_accounts_id1 FOREIGN KEY (acc1_id) REFERENCES accounts (id),
  CONSTRAINT FK_relations_accounts_id2 FOREIGN KEY (acc2_id) REFERENCES accounts (id)
);
CREATE INDEX FK_relations_accounts_id2 ON relations (acc2_id);
CREATE UNIQUE INDEX UK_relations ON relations (acc1_id, acc2_id);