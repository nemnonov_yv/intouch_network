package ru.nemnonovyuri.intouch.service;

import ru.nemnonovyuri.intouch.dao.AccountContactDao;
import ru.nemnonovyuri.intouch.dao.AccountDao;
import ru.nemnonovyuri.intouch.dao.FriendshipRequestDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import ru.nemnonovyuri.intouch.dto.*;
import ru.nemnonovyuri.intouch.exceptions.*;
import ru.nemnonovyuri.intouch.model.*;

import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

import static ru.nemnonovyuri.intouch.PasswordUtils.generateHash;
import static ru.nemnonovyuri.intouch.model.RequestStatus.DECLINED;
import static ru.nemnonovyuri.intouch.model.RequestStatus.PENDING;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
//@ContextConfiguration(locations = {"classpath:service-context.xml", "classpath:service-test-context.xml"})
public class AccountServiceTest {

    @Mock
    private AccountDao accountDao;
    @Mock
    private AccountContactDao contactDao;
    @Mock
    private FriendshipRequestDao requestDao;

    @Spy
    @InjectMocks
    private AccountService service = new AccountService();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAccount() throws Exception {
        int id = 1;
        when(accountDao.get(id)).thenReturn(makeTestAccount(id));
        assertEquals(new AccountTO(makeTestAccount(id)), service.getAccount(id));
    }

    @Test(expected = AccountNotFoundException.class)
    public void testGetNotExistedAccount() throws Exception {
        when(accountDao.get(1)).thenReturn(null);
        service.getAccount(1);
    }

    @Test
    public void testRegisterNewAccount() throws Exception {
        Account acc = makeTestAccount(null);
        AccountTO accTO = new AccountTO(acc);
        accTO.setPassword("password");
        int newId = 77;
        when(accountDao.save(any())).then(invocationOnMock -> {
            Account a = invocationOnMock.getArgument(0);
            a.setId(newId);
            return a;
        });
        String email = "my@mail.ru";
        AccountTO saved = service.registerNewAccount(accTO, email);
        assertEquals(newId, saved.getId().intValue());
    }

    @Test(expected = AccountExistsRegistrationException.class)
    public void testRegisterWithUsedLogin() throws Exception {
        AccountTO accTO = new AccountTO(makeTestAccount(null));
        accTO.setPassword("password");
        when(accountDao.getByLogin(accTO.getLogin())).thenReturn(makeTestAccount(1));
        service.registerNewAccount(accTO, "my@mail.ru");
    }

    @Test(expected = InvalidRequestException.class)
    public void testRegisterWithEmptyLogin() throws Exception {
        AccountTO to = new AccountTO(makeTestAccount(null));
        to.setLogin("    ");
        service.registerNewAccount(to, "my@mail.ru");
    }

    @Test(expected = EmptyPasswordRegistrationException.class)
    public void testRegisterWithEmptyPassword() throws Exception {
        AccountTO to = new AccountTO(makeTestAccount(null));
        to.setPassword(null);
        service.registerNewAccount(to, "my@mail.ru");
    }

    @Test(expected = InvalidRequestException.class)
    public void testRegisterWithSexIsNull() throws Exception {
        AccountTO to = new AccountTO(makeTestAccount(null));
        to.setSex(null);
        service.registerNewAccount(to, "my@mail.ru");
    }

    @Test
    public void testUpdateAccount() throws Exception {
        Account acc = makeTestAccount(1);
        AccountTO to = new AccountTO(acc);
        acc.setName("Changed");
        to.setName("Changed");
        to.setPassword("password");
        when(accountDao.get(acc.getId())).thenReturn(acc);
        service.updateAccount(to);
        verify(service).checkIdNotNull(to);
        verify(accountDao).save(acc);
    }

    @Test(expected = InvalidRequestException.class)
    public void testCheckIdNotNull() throws Exception {
        service.checkIdNotNull(new AccountTO(makeTestAccount(null)));
    }

    @Test
    public void testChangePassword() throws Exception {
        int id = 1;
        String oldPassword = "password";
        String newPassword = "12345";
        Account acc = makeTestAccount(id);
        when(accountDao.get(id)).thenReturn(acc);
        PasswordChangingTO passTO = new PasswordChangingTO(id, oldPassword, newPassword);
        service.changePassword(passTO);
        verify(accountDao).save(acc);
    }

    @Test(expected = InvalidRequestException.class)
    public void testChangePasswordOnBlankPassword() throws Exception {
        PasswordChangingTO passTO = new PasswordChangingTO(1, "password", "     ");
        service.changePassword(passTO);
        verify(accountDao, never()).save(any());
    }

    @Test(expected = AccessViolationException.class)
    public void testChangePasswordWithWrongOldPassword() throws Exception {
        int id = 1;
        Account acc = makeTestAccount(id);
        when(accountDao.get(id)).thenReturn(acc);
        PasswordChangingTO passTO = new PasswordChangingTO(id, "wrong", "12345");
        service.changePassword(passTO);
        verify(accountDao, never()).save(any());
    }

    @Test
    public void testDeleteAccount() throws Exception {
        int id = 1;
        Account acc = makeTestAccount(id);
        AccountTO to = new AccountTO(acc);
        to.setPassword("password");
        when(accountDao.get(id)).thenReturn(acc);
        service.deleteAccount(to);
        verify(accountDao).delete(acc);
    }

    @Test(expected = AccessViolationException.class)
    public void testDeleteWithWrongPassword() throws Exception {
        int id = 1;
        Account acc = makeTestAccount(id);
        AccountTO to = new AccountTO(acc);
        when(accountDao.get(id)).thenReturn(acc);
        to.setPassword("wrong");
        service.deleteAccount(to);
        verify(accountDao, never()).delete(acc);
    }

    @Test
    public void testGetAllContactInfo() throws Exception {
        Account acc = makeTestAccount(1);
        ContactInfo skypeContact = new ContactInfo(acc, ContactType.SKYPE, "echo123");
        ContactInfo icqContact = new ContactInfo(acc, ContactType.ICQ, "111-222-333");
        acc.getContacts().add(skypeContact);
        acc.getContacts().add(icqContact);
        when(accountDao.get(acc.getId())).thenReturn(acc);

        AccountContactsTO expectedTO = new AccountContactsTO(acc.getId());
        expectedTO.addContact(new ContactTO(skypeContact));
        expectedTO.addContact(new ContactTO(icqContact));
        AccountTO accTO = new AccountTO(acc);
        assertEquals(expectedTO, service.getContactInfo(accTO));
        verify(service).checkIdNotNull(accTO);
    }

    @Test
    public void testGetContactInfoByType() throws Exception {
        Account acc = makeTestAccount(1);
        ContactInfo skypeContact = new ContactInfo(acc, ContactType.SKYPE, "echo123");
        ContactInfo icqContact = new ContactInfo(acc, ContactType.ICQ, "111-222-333");
        acc.getContacts().add(skypeContact);
        acc.getContacts().add(icqContact);
        when(accountDao.get(acc.getId())).thenReturn(acc);

        AccountContactsTO expectedTO = new AccountContactsTO(acc.getId());
        expectedTO.addContact(new ContactTO(skypeContact));
        AccountTO accTO = new AccountTO(acc);
        assertEquals(expectedTO, service.getContactInfoByType(accTO, ContactType.SKYPE));
        verify(service).checkIdNotNull(accTO);
    }

    @Test
    public void testAddNewContact() throws Exception {
        Account acc = makeTestAccount(1);
        ContactInfo skypeContact = new ContactInfo(acc, ContactType.SKYPE, "echo123");
        skypeContact.setId(1);
        ContactInfo icqContact = new ContactInfo(acc, ContactType.ICQ, "111-222-333");
        icqContact.setId(2);
        acc.getContacts().add(skypeContact);
        acc.getContacts().add(icqContact);
        when(accountDao.get(acc.getId())).thenReturn(acc);

        ContactInfo addrContact = new ContactInfo(acc, ContactType.HOME_ADDR, "Red Square");
        Set<ContactInfo> expected = new HashSet<>(Arrays.asList(skypeContact, icqContact, addrContact));

        AccountContactsTO updateTO = new AccountContactsTO(acc.getId());
        updateTO.addContact(new ContactTO(skypeContact));
        updateTO.addContact(new ContactTO(icqContact));
        updateTO.addContact(new ContactTO(addrContact));
        service.updateContacts(updateTO);
        assertEquals(expected, acc.getContacts());
    }

    @Test
    public void testUpdateContact() throws Exception {
        Account acc = makeTestAccount(1);
        ContactInfo skypeContact = new ContactInfo(acc, ContactType.SKYPE, "echo123");
        skypeContact.setId(1);
        ContactInfo icqContact = new ContactInfo(acc, ContactType.ICQ, "111-222-333");
        icqContact.setId(2);
        acc.getContacts().add(skypeContact);
        acc.getContacts().add(icqContact);
        when(accountDao.get(acc.getId())).thenReturn(acc);

        String newValue = "111-777-333";
        ContactInfo updated = new ContactInfo(acc, ContactType.ICQ, newValue);
        updated.setId(2);
        Set<ContactInfo> expected = new HashSet<>(Arrays.asList(skypeContact, updated));

        AccountContactsTO updateTO = new AccountContactsTO(acc.getId());
        updateTO.addContact(new ContactTO(skypeContact));
        ContactTO updatedContact = new ContactTO(icqContact);
        updatedContact.setValue(newValue);
        updateTO.addContact(updatedContact);
        service.updateContacts(updateTO);
        assertEquals(expected, acc.getContacts());
    }

    @Test
    public void testDeleteContact() throws Exception {
        Account acc = makeTestAccount(1);
        ContactInfo skypeContact = new ContactInfo(acc, ContactType.SKYPE, "echo123");
        skypeContact.setId(1);
        ContactInfo icqContact = new ContactInfo(acc, ContactType.ICQ, "111-222-333");
        icqContact.setId(2);
        acc.getContacts().add(skypeContact);
        acc.getContacts().add(icqContact);
        when(accountDao.get(acc.getId())).thenReturn(acc);

        Set<ContactInfo> expected = new HashSet<>(Arrays.asList(icqContact));

        AccountContactsTO deleteTO = new AccountContactsTO(acc.getId());
        deleteTO.addContact(new ContactTO(skypeContact));
        service.deleteContacts(deleteTO);
        assertEquals(expected, acc.getContacts());
    }

    @Test
    public void testSendFriendshipRequest() throws Exception {
        int senderId = 1;
        Account sender = makeTestAccount(senderId);
        int recipientId = 2;
        Account recipient = makeTestAccount(recipientId);

        when(accountDao.get(senderId)).thenReturn(sender);
        when(accountDao.get(recipientId)).thenReturn(recipient);
        when(requestDao.getByAccounts(sender, recipient)).thenReturn(null);
        when(requestDao.getByAccounts(recipient, sender)).thenReturn(null);

        AccountTO senderTO = new AccountTO(sender);
        AccountTO recipientTO = new AccountTO(recipient);
        service.sendFriendshipRequest(senderTO, recipientTO);
        verify(service).checkIdNotNull(senderTO, recipientTO);
        verify(requestDao).save(new FriendshipRequest(sender, recipient));
    }

    @Test
    public void testGetPendingIncomeFriendshipRequests() throws Exception {
        int id = 1;
        Account recipient = makeTestAccount(id);
        Set<FriendshipRequest> requests = new HashSet<>();
        requests.add(makeFriendshipRequest(2, id, true));
        requests.add(makeFriendshipRequest(3, id, false));
        requests.add(makeFriendshipRequest(4, id, true));
        recipient.setIncomeRequests(requests);
        List<FriendshipRequestTO> expected = requests.stream()
                .filter(request -> request.getStatus() == RequestStatus.PENDING)
                .map(FriendshipRequestTO::new)
                .collect(Collectors.toList());

        when(accountDao.get(id)).thenReturn(recipient);
        AccountTO recipientTO = new AccountTO(recipient);
        List<FriendshipRequestTO> actual = service.getPendingIncomeFriendshipRequests(recipientTO);
        assertEquals(expected, actual);
        verify(service).checkIdNotNull(recipientTO);
    }

    @Test
    public void testGetPendingOutcomeFriendshipRequests() throws Exception {
        int id = 1;
        Account sender = makeTestAccount(id);
        Set<FriendshipRequest> requests = new HashSet<>();
        requests.add(makeFriendshipRequest(2, id, true));
        requests.add(makeFriendshipRequest(3, id, false));
        requests.add(makeFriendshipRequest(4, id, true));
        sender.setOutcomeRequests(requests);
        List<FriendshipRequestTO> expected = requests.stream()
                .filter(request -> request.getStatus() == RequestStatus.PENDING)
                .map(FriendshipRequestTO::new)
                .collect(Collectors.toList());

        when(accountDao.get(id)).thenReturn(sender);
        AccountTO senderTO = new AccountTO(sender);
        List<FriendshipRequestTO> actual = service.getPendingOutcomeFriendshipRequests(senderTO);
        assertEquals(expected, actual);
        verify(service).checkIdNotNull(senderTO);
    }

    @Test
    public void testAcceptFriendshipRequest() throws Exception {
        FriendshipRequest req = makeFriendshipRequest(1, 2, true);
        Account sender = req.getSender();
        Account recipient = req.getRecipient();
        when(requestDao.get(req.getId())).thenReturn(req);
        when(accountDao.get(sender.getId())).thenReturn(sender);
        when(accountDao.get(recipient.getId())).thenReturn(recipient);
        FriendshipRequestTO reqTO = new FriendshipRequestTO(req);
        service.acceptFriendshipRequest(reqTO);
        verify(service).checkIdNotNull(reqTO);
        verify(requestDao).delete(req);
        assertTrue(sender.getFriends().contains(recipient));
        assertTrue(recipient.getFriends().contains(sender));
    }

    @Test
    public void testAcceptNotExistedFriendshipRequest() throws Exception {
        FriendshipRequest req = makeFriendshipRequest(1, 2, true);
        Account sender = req.getSender();
        Account recipient = req.getRecipient();
        when(accountDao.get(sender.getId())).thenReturn(sender);
        when(accountDao.get(recipient.getId())).thenReturn(recipient);
        when(requestDao.get(req.getId())).thenReturn(null);

        FriendshipRequestTO reqTO = new FriendshipRequestTO(req);
        service.acceptFriendshipRequest(reqTO);
        verify(service).checkIdNotNull(reqTO);
        verify(accountDao, never()).save(sender);
        verify(accountDao, never()).save(recipient);
        verify(requestDao, never()).delete(any());
    }

    @Test
    public void testAcceptFakeFriendshipRequest() throws Exception {
        FriendshipRequest req = makeFriendshipRequest(1, 2, true);
        when(requestDao.get(req.getId())).then(invocationOnMock -> {
            FriendshipRequest fr = makeFriendshipRequest(1, 33, true);
            fr.setId(invocationOnMock.getArgument(0));
            return fr;
        });
        Account sender = req.getSender();
        Account recipient = req.getRecipient();
        when(accountDao.get(sender.getId())).thenReturn(sender);
        when(accountDao.get(recipient.getId())).thenReturn(recipient);

        FriendshipRequestTO reqTO = new FriendshipRequestTO(req);
        service.acceptFriendshipRequest(reqTO);
        verify(service).checkIdNotNull(reqTO);
        verify(accountDao, never()).save(sender);
        verify(accountDao, never()).save(recipient);
        verify(requestDao, never()).delete(any());
    }

    @Test
    public void testDeclineFriendshipRequest() throws Exception {
        FriendshipRequest req = makeFriendshipRequest(1, 2, true);
        Account recipient = req.getRecipient();
        when(requestDao.get(req.getId())).thenReturn(req);
        when(accountDao.get(recipient.getId())).thenReturn(recipient);

        FriendshipRequestTO reqTO = new FriendshipRequestTO(req);
        service.declineFriendshipRequest(reqTO);
        req.setStatus(DECLINED);
        verify(service).checkIdNotNull(reqTO);
        verify(requestDao).save(
                argThat(obj -> obj.getSender().equals(req.getSender())
                        && obj.getRecipient().equals(recipient)
                        && obj.getStatus() == DECLINED)
        );
    }

    @Test
    public void testRevokeFriendshipRequest() throws Exception {
        FriendshipRequest req = makeFriendshipRequest(1, 2, true);
        Account sender = req.getSender();
        when(requestDao.get(req.getId())).thenReturn(req);
        when(accountDao.get(sender.getId())).thenReturn(sender);

        FriendshipRequestTO reqTO = new FriendshipRequestTO(req);
        service.revokeFriendshipRequest(reqTO);
        verify(service).checkIdNotNull(reqTO);
        verify(requestDao).delete(req);
    }

    @Test
    public void testDeleteFriend() throws Exception {
        Account acc = makeTestAccount(1);
        Account friend = makeTestAccount(22);
        acc.getFriends().add(friend);
        acc.getFriends().add(makeTestAccount(33));
        when(accountDao.get(acc.getId())).thenReturn(acc);
        when(accountDao.get(friend.getId())).thenReturn(friend);

        AccountTO accTO = new AccountTO(acc);
        AccountTO friendTO = new AccountTO(friend);
        service.deleteFriend(accTO, friendTO);
        verify(service).checkIdNotNull(accTO, friendTO);
        verify(accountDao).save(acc);
        assertFalse(acc.getFriends().contains(friend));
    }

    @Test
    public void testGetFriends() throws Exception {
        Account acc = makeTestAccount(1);
        List<Account> friends = new ArrayList<>();
        friends.add(makeTestAccount(22));
        friends.add(makeTestAccount(33));
        friends.add(makeTestAccount(44));
        List<AccountTO> expected = new ArrayList<>();
        for (Account a : friends) {
            expected.add(new AccountTO(a));
            acc.getFriends().add(a);
        }

        AccountTO accTO = new AccountTO(acc);
        when(accountDao.get(acc.getId())).thenReturn(acc);
        assertEquals(expected, service.getFriends(accTO));
        verify(service).checkIdNotNull(accTO);
    }

    @After
    public void tearDown() throws Exception {
    }

    private Account makeTestAccount(Integer id) {
        Account acc = new Account();
        acc.setId(id);
        acc.setLogin("Test_" + id);
        acc.setName("Test Name " + id);
        acc.setBirthDate(Date.valueOf("2001-01-01"));
        acc.setSex(Sex.MALE);
        acc.setPasswordHash(generateHash("password"));
        return acc;
    }

    private List<ContactInfo> makeTestContactList(Account acc) {
        List<ContactInfo> list = new ArrayList<>();
        ContactInfo c = new ContactInfo(acc, ContactType.EMAIL, "dont@mail.me");
        c.setId(1);
        list.add(c);
        c = new ContactInfo(acc, ContactType.SKYPE, "echo123");
        c.setId(2);
        list.add(c);
        return list;
    }

    private AccountContactsTO makeContactsTO(List<ContactInfo> list) {
        AccountContactsTO contactsTO = null;
        for (ContactInfo ci : list) {
            if (contactsTO == null) {
                contactsTO = new AccountContactsTO(ci.getAccount().getId());
            }
            contactsTO.addContact(new ContactTO(ci));
        }
        return contactsTO;
    }

    private FriendshipRequest makeFriendshipRequest(int senderId, int recipientId, boolean isPending) {
        Account sender = makeTestAccount(senderId);
        Account recipient = makeTestAccount(recipientId);
        FriendshipRequest req = new FriendshipRequest(sender, recipient, isPending ? PENDING : DECLINED);
        req.setId(senderId * 100 + recipientId);
        return req;
    }
}