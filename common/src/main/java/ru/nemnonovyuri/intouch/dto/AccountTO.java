package ru.nemnonovyuri.intouch.dto;

import ru.nemnonovyuri.intouch.model.Account;
import ru.nemnonovyuri.intouch.model.AccountStatus;
import ru.nemnonovyuri.intouch.model.Sex;

import java.sql.Date;

public class AccountTO extends IdentityTO {
    private String login;
    private String name;
    private Date birthDate;
    private Sex sex;
    private String password;
    private String photo;
    private AccountStatus status;

    public AccountTO() {
    }

    public AccountTO(Integer id, String login, String name, Date birthDate, Sex sex) {
        super(id);
        this.login = login;
        this.name = name;
        this.birthDate = birthDate;
        this.sex = sex;
        this.status = AccountStatus.ACTIVE;
    }

    public AccountTO(Account account) {
        this(account.getId(), account.getLogin(), account.getName(), account.getBirthDate(), account.getSex());
        status = account.getStatus();
        photo = account.getPhoto();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AccountTO{" +
                "id=" + getId() +
                ", login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", birthDate=" + birthDate +
                ", sex=" + sex +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountTO accountTO = (AccountTO) o;

        if (!getLogin().equals(accountTO.getLogin())) return false;
        if (!getName().equals(accountTO.getName())) return false;
        if (getBirthDate() != null ? !getBirthDate().equals(accountTO.getBirthDate()) : accountTO.getBirthDate() != null)
            return false;
        if (getSex() != accountTO.getSex()) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = getLogin().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + (getBirthDate() != null ? getBirthDate().hashCode() : 0);
        result = 31 * result + getSex().hashCode();
        return result;
    }
}
