DROP ALL OBJECTS;

CREATE TABLE accounts (
  id        INT(11)      NOT NULL AUTO_INCREMENT
  COMMENT 'Account ID',
  login     VARCHAR(100) NOT NULL
  COMMENT 'User login',
  pass      VARCHAR(255) NOT NULL
  COMMENT 'Password hash',
  name      VARCHAR(255) NOT NULL
  COMMENT 'User name',
  birthdate DATE                  DEFAULT NULL
  COMMENT 'Birth date',
  sex       INT(1)       NOT NULL,
  photo     VARCHAR(255)          DEFAULT NULL,
  status    INT(1)       NOT NULL DEFAULT 0
  COMMENT 'Account status flag',
  PRIMARY KEY (id),
  UNIQUE (login)
);

CREATE TABLE groups (
  id          INT(11)      NOT NULL AUTO_INCREMENT
  COMMENT 'ID',
  name        VARCHAR(100) NOT NULL
  COMMENT 'Group unique name',
  title       VARCHAR(255) NOT NULL
  COMMENT 'Group title',
  description TEXT         NOT NULL
  COMMENT 'Group description',
  photo       VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE (name)
);

CREATE TABLE contact_info (
  id     INT(11)      NOT NULL AUTO_INCREMENT
  COMMENT 'ID',
  acc_id INT(11)      NOT NULL
  COMMENT 'Account ID',
  type   TINYINT      NOT NULL
  COMMENT 'Contact type',
  value  VARCHAR(255) NOT NULL
  COMMENT 'Contact value',
  PRIMARY KEY (id),
  CONSTRAINT FK_contact_info_accounts_id FOREIGN KEY (acc_id)
  REFERENCES accounts (id) ON DELETE RESTRICT ON UPDATE RESTRICT
);

CREATE TABLE group_join_requests (
  ID       INT(11) NOT NULL AUTO_INCREMENT
  COMMENT 'ID',
  group_id INT(11) NOT NULL
  COMMENT 'Group ID',
  acc_id   INT(11) NOT NULL
  COMMENT 'Account ID',
  active   TINYINT NOT NULL
  COMMENT 'Request active',
  PRIMARY KEY (ID),
  CONSTRAINT FK_group_join_requests_accounts_id FOREIGN KEY (acc_id)
  REFERENCES accounts (id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_group_join_requests_groups_id FOREIGN KEY (group_id)
  REFERENCES groups (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE group_members (
  ID       INT(11) NOT NULL AUTO_INCREMENT
  COMMENT 'ID',
  group_id INT(11) NOT NULL
  COMMENT 'Group ID',
  acc_id   INT(11) NOT NULL
  COMMENT 'Account ID',
  role     TINYINT NOT NULL
  COMMENT 'User role',
  UNIQUE (group_id, acc_id),
  CONSTRAINT FK_group_members_accounts_id FOREIGN KEY (acc_id)
  REFERENCES accounts (id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_group_members_groups_id FOREIGN KEY (group_id)
  REFERENCES groups (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE relation_requests (
  ID           INT(11) NOT NULL AUTO_INCREMENT
  COMMENT 'ID',
  sender_id    INT(11) NOT NULL
  COMMENT 'Sender account ID',
  recipient_id INT(11) NOT NULL
  COMMENT 'Recipient account ID',
  status       TINYINT NOT NULL
  COMMENT 'Request status',
  PRIMARY KEY (ID),
  CONSTRAINT FK_relation_requests_recipient_account_id FOREIGN KEY (recipient_id)
  REFERENCES accounts (id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_relation_requests_sender_account_id FOREIGN KEY (sender_id)
  REFERENCES accounts (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE relations (
  ID      INT(11) NOT NULL AUTO_INCREMENT
  COMMENT 'ID',
  acc1_id INT(11) NOT NULL
  COMMENT 'First account ID',
  acc2_id INT(11) NOT NULL
  COMMENT 'Second account ID',
  UNIQUE (acc1_id, acc2_id),
  CONSTRAINT FK_relations_accounts_id1 FOREIGN KEY (acc1_id)
  REFERENCES accounts (id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_relations_accounts_id2 FOREIGN KEY (acc2_id)
  REFERENCES accounts (id) ON DELETE RESTRICT ON UPDATE RESTRICT
);