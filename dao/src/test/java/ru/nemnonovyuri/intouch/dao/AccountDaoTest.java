package ru.nemnonovyuri.intouch.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nemnonovyuri.intouch.DaoTestData;
import ru.nemnonovyuri.intouch.model.*;

import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-test-context.xml"})
public class AccountDaoTest {
    @Autowired
    private AccountDao dao;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testSave() throws Exception {
        Account acc = new Account();
        acc.setLogin("Vasya");
        acc.setPasswordHash("$$$VasyaPassword$$$");
        acc.setName("Vasiliy Pupkin");
        acc.setBirthDate(Date.valueOf("2000-01-01"));
        acc.setSex(Sex.MALE);
        Account saved = dao.save(acc);
        Assert.assertEquals(saved, dao.get(saved.getId()));
    }

    @Test
    public void testGetById() throws Exception {
        Account a = dao.get(3);
        Assert.assertEquals(DaoTestData.ACCOUNT_ID3, dao.get(3));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testUpdate() throws Exception {
        Account acc = dao.get(4);
        acc.setSex(Sex.MALE);
        dao.save(acc);
        Account updated = dao.get(4);
        assertEquals(acc.getSex(), updated.getSex());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void testDelete() throws Exception {
        int deletedId = 4;
        Account accBeforeDel = dao.get(deletedId);
        dao.delete(accBeforeDel);
        Account acc = dao.get(deletedId);
        assertTrue(acc.getStatus() == AccountStatus.DELETED && accBeforeDel.getStatus() == AccountStatus.ACTIVE);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Account> all = new ArrayList<>();
        all.add(DaoTestData.ACCOUNT_ID4);
        all.add(DaoTestData.ACCOUNT_ID1);
        all.add(DaoTestData.ACCOUNT_ID2);
        all.add(DaoTestData.ACCOUNT_ID3);
        assertEquals(all, dao.getAll());
    }

    @Test
    public void testGetQueryAllResultCount() throws Exception {
        assertEquals(4L, (long) dao.getQueryAllResultCount());
    }

    @Test
    public void testGetByLogin() throws Exception {
        Assert.assertEquals(DaoTestData.ACCOUNT_ID3, dao.getByLogin("mary"));
    }

    @Test
    public void testGetByName() throws Exception {
        List<Account> expected = new ArrayList<>(Arrays.asList(DaoTestData.ACCOUNT_ID2, DaoTestData.ACCOUNT_ID3));
        List<Account> actual = dao.getByName("Mari");
        assertEquals(expected, actual);
    }

    @Test
    public void testGetQueryByNameResultCount() throws Exception {
        assertEquals(2L, (long) dao.getQueryByNameResultCount("Mari"));
    }

    @Test
    @Transactional
    public void testGetWithFriends() throws Exception {
        Set<Account> friends = new HashSet<>();
        friends.add(DaoTestData.ACCOUNT_ID1);
        friends.add(DaoTestData.ACCOUNT_ID4);
        Assert.assertEquals(friends, dao.get(DaoTestData.ACCOUNT_ID3.getId()).getFriends());
    }

    @Test
    @Transactional
    public void testGetContacts() throws Exception {
        Set<ContactInfo> contacts = new HashSet<>();
        contacts.add(DaoTestData.CONTACT_INFO_ID1);
        contacts.add(DaoTestData.CONTACT_INFO_ID3);
        Assert.assertEquals(contacts, dao.get(DaoTestData.ACCOUNT_ID1.getId()).getContacts());
    }

    @Test
    @Transactional
    public void testGetMembership() throws Exception {
        Set<Group> expected = new HashSet<>();
        expected.add(DaoTestData.GROUP_ID1);
        expected.add(DaoTestData.GROUP_ID3);
        Assert.assertEquals(expected, dao.get(DaoTestData.ACCOUNT_ID3.getId()).getGroups()
                .stream().map(GroupMember::getGroup).collect(Collectors.toSet()));
    }

    @After
    public void tearDown() throws Exception {
    }
}