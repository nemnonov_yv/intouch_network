package ru.nemnonovyuri.intouch.controllers;

import ru.nemnonovyuri.intouch.dto.AccountTO;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

public abstract class BaseController {

    protected void setTitle(ModelAndView view, String title) {
        view.addObject("pageTitle", title);
    }

    protected AccountTO getAccountFromSession(HttpServletRequest request) {
        return (AccountTO) request.getSession().getAttribute("account");
    }
}
