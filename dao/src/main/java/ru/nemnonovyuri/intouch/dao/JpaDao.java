package ru.nemnonovyuri.intouch.dao;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.lang.reflect.ParameterizedType;

public abstract class JpaDao<E> {
    protected Class<E> entityClass;

    @PersistenceContext
    protected EntityManager entityManager;

    public JpaDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[0];
    }

    @Transactional
    public E save(E entity) {
        E merged = entityManager.merge(entity);
        return merged;
    }

    @Transactional
    public void delete(E entity) {
        entityManager.remove(entityManager.merge(entity));
    }

    public E get(Integer id) {
        return entityManager.find(entityClass, id);
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    protected CriteriaQuery<E> getSelectQuery(String orderBy) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = cb.createQuery(entityClass);
        Root<E> root = query.from(entityClass);
        query.select(root);
        if (orderBy != null) {
            query.orderBy(cb.asc(root.get(orderBy)));
        }
        return query;
    }

    protected CriteriaQuery<Long> getCountQuery() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = cb.createQuery(Long.class);
        query.select(cb.count((query.from(entityClass))));
        return query;
    }

    /**
     * Utility shortcut method to retrieve query single root
     *
     * @param query
     * @return query root
     */
    protected Root<E> getRoot(CriteriaQuery<?> query) {
        return (Root<E>) query.getRoots().iterator().next();
    }
}
