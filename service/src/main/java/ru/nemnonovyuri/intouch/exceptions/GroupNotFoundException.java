package ru.nemnonovyuri.intouch.exceptions;

public class GroupNotFoundException extends ServiceException {
    public GroupNotFoundException() {
        super();
    }

    public GroupNotFoundException(int id) {
        super("id=" + id);
    }

    public GroupNotFoundException(String message) {
        super(message);
    }

    public GroupNotFoundException(Throwable cause) {
        super(cause);
    }

    public GroupNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
