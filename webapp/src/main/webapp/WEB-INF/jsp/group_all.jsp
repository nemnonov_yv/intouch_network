<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@page import="AccountHelper" %>--%>
<%--<%@page import="GroupHelper" %>--%>
<%@page import="ru.nemnonovyuri.intouch.helpers.GroupHelper" %>
<jsp:include page="header.jsp"/>
<div class="col-md-4 col-md-offset-4">
    <div class="row">
        <div class="col-md-6">
            <h4>Groups</h4>
        </div>
        <div class="col-md-6">
            <a href="/groups/create" class="btn btn-sm btn-info">Create new</a>
        </div>
    </div>
    <c:choose>
        <c:when test="${groupList.size() > 0}">
            <c:forEach var="group" items="${groupList}">
                <div class="row">
                    <div class="col-md-3">
                        <div class="thumbnail">
                            <a href="/groups/view/${group.id}"><img class="thumbnail"
                                                                    src="${GroupHelper.getGroupImageUrl(group)}"></a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <h4><a href="/groups/view/${group.id}">${group.title}</a></h4>

                        <p>${group.description}</p>
                    </div>
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            No groups was found.
        </c:otherwise>
    </c:choose>
    <%--<c:if test="${groupList.size() > 0}">--%>
    <%----%>
    <%--</c:if>--%>

</div>
<jsp:include page="footer.jsp"/>