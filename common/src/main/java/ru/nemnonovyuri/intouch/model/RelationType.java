package ru.nemnonovyuri.intouch.model;

public enum RelationType {
    FRIEND(0), BROTHER(1), SISTER(2), MOTHER(3), FATHER(4), SON(5), DAUGHTER(6), GRANDMA(7), GRANDPA(8);

    private int numeric;

    RelationType(int i) {
        numeric = i;
    }

    public static RelationType valueOf(int numeric) {
        for (RelationType type : RelationType.values()) {
            if (type.getNumeric() == numeric) {
                return type;
            }
        }
        throw new RuntimeException("Invalid relation type numeric presentation");
    }

    public int getNumeric() {
        return numeric;
    }
}
