<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="header.jsp"/>
<div class="col-md-2">
    <h4>My profile <a href="/profile/edit" class="btn btn-default"><i class="glyphicon glyphicon-pencil"></i></a></h4>

    <div class="thumbnail">
        <img src="${photoUrl}"/>

        <div class="text-center"><h4>${account.name}</h4></div>
    </div>
    <c:if test="${contacts.size() > 0}">
        <dl>
            <c:forEach var="contact" items="${contacts}">
                <dt>${contact.type}</dt>
                <dd>${contact.value}</dd>
            </c:forEach>
        </dl>
    </c:if>
</div>
<div class="col-md-9">
    <h4>Wall</h4>
</div>
<jsp:include page="footer.jsp"/>