package ru.nemnonovyuri.intouch.model.converters;

import ru.nemnonovyuri.intouch.model.RequestStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class RequestStatusJpaConverter implements AttributeConverter<RequestStatus, Integer> {
    @Override
    public Integer convertToDatabaseColumn(RequestStatus status) {
        return status.getNumeric();
    }

    @Override
    public RequestStatus convertToEntityAttribute(Integer integer) {
        return RequestStatus.valueOf(integer);
    }
}
