package ru.nemnonovyuri.intouch.dao;

import ru.nemnonovyuri.intouch.model.Account;
import ru.nemnonovyuri.intouch.model.Group;
import ru.nemnonovyuri.intouch.model.GroupMember;
import ru.nemnonovyuri.intouch.model.MemberRole;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Repository
public class GroupDao extends JpaDao<Group> {
    public List<Group> getAll() {
        return entityManager.createQuery(getSelectQuery(null)).getResultList();
    }

    public List<Group> getByTitle(String title) {
//        return entityManager
//                .createQuery("SELECT g FROM Group g WHERE g.title LIKE :title")
//                .setParameter("title", "%" + title + "%")
//                .getResultList();
        return getByTitle(title, 0, 0);
    }

    public List<Group> getByTitle(String title, int limit, int offset) {
//        return entityManager
//                .createQuery("SELECT g FROM Group g WHERE g.title LIKE :title")
//                .setParameter("title", "%" + title + "%")
//                .getResultList();
        CriteriaQuery<Group> selectQuery = getSelectQuery("title");
        selectQuery.where(entityManager.getCriteriaBuilder().like(
                getRoot(selectQuery).get("title"), "%" + title + "%"));
        TypedQuery<Group> query = entityManager.createQuery(selectQuery);
        return limit == 0 ? query.getResultList()
                : query.setFirstResult(offset).setMaxResults(limit).getResultList();
    }

    public Integer getQueryByTitleResultCount(String title) {
        CriteriaQuery<Long> countQuery = getCountQuery();
        countQuery.where(entityManager.getCriteriaBuilder().like(getRoot(countQuery).get("title"), "%" + title + "%"));
        return entityManager.createQuery(countQuery).getSingleResult().intValue();
    }

    @Transactional
    public void addMember(Group gr, Account acc, MemberRole role) {
        Group group = get(gr.getId());
        GroupMember gm = new GroupMember(entityManager.merge(acc), group, role);
        entityManager.persist(gm);
        //group.getMembers().add(gm);
        save(group);
    }

    public List<Account> getMembersByRole(Group gr, MemberRole role) {
        return entityManager
                .createQuery("SELECT gm.membership.member FROM GroupMember gm WHERE gm.membership.group=:gr AND gm.membership.role=:role")
                .setParameter("gr", gr)
                .setParameter("role", role)
                .getResultList();
    }

    public MemberRole getMemberRole(Group gr, Account acc) {
        return (MemberRole) entityManager
                .createQuery("SELECT gm.membership.role FROM GroupMember gm WHERE gm.membership.group=:gr AND gm.membership.member=:acc")
                .setParameter("gr", gr)
                .setParameter("acc", acc)
                .getSingleResult();
    }
}
