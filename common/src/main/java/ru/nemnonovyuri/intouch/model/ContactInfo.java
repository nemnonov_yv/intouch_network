package ru.nemnonovyuri.intouch.model;

import ru.nemnonovyuri.intouch.model.converters.ContactTypeJpaConverter;

import javax.persistence.*;

@Entity
@Table(name = "contact_info")
//@Access(value = AccessType.PROPERTY)
public class ContactInfo extends AbstractIdentity {
    @ManyToOne
    @JoinColumn(name = "acc_id")
    private Account account;

    @Column(name = "type")
    @Convert(converter = ContactTypeJpaConverter.class)
    private ContactType type;

    @Column(name = "value")
    private String value;

    public ContactInfo() {
    }

    public ContactInfo(ContactType type, String value) {
        this(null, type, value);
    }

    public ContactInfo(Account account, ContactType type, String value) {
        this.account = account;
        this.type = type;
        this.value = value;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public ContactType getType() {
        return type;
    }

    public void setType(ContactType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContactInfo that = (ContactInfo) o;

        if (!getAccount().equals(that.getAccount())) return false;
        if (getType() != that.getType()) return false;
        return getValue().equals(that.getValue());
    }

    @Override
    public int hashCode() {
        int result = getAccount() == null ? 0 : getAccount().hashCode();
        result = 31 * result + (getAccount() == null ? 0 : getType().hashCode());
        result = 31 * result + (getValue() == null ? 0 : getValue().hashCode());
        return result;
    }
}
