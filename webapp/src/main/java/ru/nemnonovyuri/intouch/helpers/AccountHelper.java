package ru.nemnonovyuri.intouch.helpers;

import ru.nemnonovyuri.intouch.dto.AccountTO;
import ru.nemnonovyuri.intouch.model.Sex;

import static ru.nemnonovyuri.intouch.util.StringUtils.isEmpty;

public class AccountHelper {
    public static String getAccountPhotoUrl(AccountTO acc) {
        if (isEmpty(acc.getPhoto())) {
            return acc.getSex() == Sex.FEMALE ? "/static/i/user_female.png" : "/static/i/user_male.png";
        } else {
            return ImageUploads.getImageUrl(acc, acc.getPhoto(), ImageUploads.Type.USER_PHOTO);
        }
    }

    public static String getAccountProfileUrl(AccountTO acc) {
        return "/users/view/" + acc.getId();
    }
}