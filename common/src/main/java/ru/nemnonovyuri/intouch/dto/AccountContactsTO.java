package ru.nemnonovyuri.intouch.dto;

import java.util.HashSet;
import java.util.Set;

public class AccountContactsTO {
    private Integer accountId;
    private Set<ContactTO> contacts;

    public AccountContactsTO(Integer accountId) {
        this(accountId, new HashSet<>());
    }

    public AccountContactsTO(Integer accountId, Set<ContactTO> contacts) {
        this.accountId = accountId;
        this.contacts = contacts;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Set<ContactTO> getContacts() {
        return contacts;
    }

    public void setContacts(Set<ContactTO> contacts) {
        this.contacts = contacts;
    }

    public void addContact(ContactTO contactTO) {
        contacts.add(contactTO);
    }

    @Override
    public String toString() {
        return "AccountContactsTO{" +
                "accountId=" + accountId +
                ", contacts=" + contacts +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountContactsTO that = (AccountContactsTO) o;

        if (!getAccountId().equals(that.getAccountId())) return false;
        return getContacts().equals(that.getContacts());
    }

    @Override
    public int hashCode() {
        int result = getAccountId().hashCode();
        result = 31 * result + getContacts().hashCode();
        return result;
    }
}
