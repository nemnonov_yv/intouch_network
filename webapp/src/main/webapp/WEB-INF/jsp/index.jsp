<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="header.jsp"/>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <h3>Welcome</h3>

        <p>Please login or <a href="<c:url value="/signup"/>">register</a></p>
        <jsp:include page="login_form.jsp"/>
    </div>
</div>
<jsp:include page="footer.jsp"/>