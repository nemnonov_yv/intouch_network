package ru.nemnonovyuri.intouch.service;

import ru.nemnonovyuri.intouch.dto.IdentityTO;
import ru.nemnonovyuri.intouch.exceptions.InvalidRequestException;

public abstract class BaseService {
    protected void checkIdNotNull(IdentityTO... tos) {
        for (IdentityTO to : tos) {
            if (to.getId() == null) {
                throw new InvalidRequestException(to.toString());
            }
        }
    }
}
