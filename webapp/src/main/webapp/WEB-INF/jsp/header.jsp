<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>${pageTitle}</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="/static/jquery-ui/jquery-ui.min.css"/>
    <script src="/static/jquery-ui/external/jquery/jquery.js"></script>
    <script src="/static/jquery-ui/jquery-ui.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>
    <script src="/static/js/profile_edit.js"></script>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">InTouch</a>
        </div>
        <c:if test="${account != null}">
            <jsp:include page="header_menu.jsp"/>
            <p class="navbar-text navbar-right">
                <a href="<c:url value="/profile"/>">${account.name}</a> (<a href="<c:url value="/logout"/>">logout</a>)
            </p>
            <jsp:include page="search_form.jsp"/>
        </c:if>
    </div>
</nav>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
