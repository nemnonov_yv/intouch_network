package ru.nemnonovyuri.intouch.helpers;

import ru.nemnonovyuri.intouch.dto.IdentityTO;
import org.springframework.web.multipart.MultipartFile;
import ru.nemnonovyuri.intouch.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ImageUploads {
    private static final String imageUrlPrefix = "/images/";
    //directories to store uploaded files
    //TODO place to config
    private static final String UPLOADS_DIR = "/usr/uploads/";
    private static final String PHOTO_SUBDIR = "p/";
    private static final String GROUP_SUBDIR = "g/";
    private static final String MSG_SUBDIR = "m/";

    public static String upload(HttpServletRequest request, String param, IdentityTO to, Type type) throws IOException, ServletException {
        if (to.getId() == null) {
            return null;
        }
        Part part = request.getPart(param);
        String ext = getUploadedFileExtension(part.getSubmittedFileName());
        if (ext == null) {
            return null;
        }
        String newFilename = String.valueOf(System.currentTimeMillis()) + ext;
        saveFile(part, getRealPathToImage(String.valueOf(to.getId()), type), newFilename);
        return newFilename;
    }

    public static String upload(MultipartFile file, IdentityTO to, Type type) {
        if (to.getId() == null || file.isEmpty()) {
            return null;
        }
        String ext = getUploadedFileExtension(file.getOriginalFilename());
        if (ext == null) {
            return null;
        }
        String newFilename = String.valueOf(System.currentTimeMillis()) + ext;
        try {
            saveFile(file, getRealPathToImage(String.valueOf(to.getId()), type), newFilename);
        } catch (IllegalStateException | IOException e) {
            return null;
        }
        return newFilename;
    }

    public static void delete(IdentityTO to, String fileName, Type type) throws IOException {
        if (to.getId() != null && !StringUtils.isEmpty(fileName)) {
            Files.delete(Paths.get(getRealPathToImage(String.valueOf(to.getId()), type) + fileName));
        }
    }

    public static String convertUrlToPath(String url) {
        if (!url.startsWith(imageUrlPrefix)) {
            return null;
        }
        return UPLOADS_DIR + url.substring(imageUrlPrefix.length());
    }

    public static String getImageUrl(IdentityTO to, String fileName, Type type) {
        return imageUrlPrefix + getRealPathToImage(String.valueOf(to.getId()), type).substring(UPLOADS_DIR.length()) + fileName;
    }

    private static String getRealPathToImage(String id, Type type) {
        return UPLOADS_DIR + type.getSubdir() + id + (StringUtils.isEmpty(id) ? "" : "/");
    }

    private static void saveFile(Part part, String path, String fileName) throws IOException {
        new File(path).mkdir();
        part.write(path + fileName);
    }

    private static void saveFile(MultipartFile multipartFile, String path, String fileName) throws IOException {
        new File(path).mkdir();
        File file = new File(path + fileName);
        multipartFile.transferTo(file);
    }

    private static String getUploadedFileExtension(String fileName) {
        if (StringUtils.isEmpty(fileName)) {
            return null;
        }
        int p = fileName.lastIndexOf(".");
        return (p > 0 && p < fileName.length() - 1) ? fileName.substring(p) : "";
    }

    public enum Type {
        USER_PHOTO(PHOTO_SUBDIR), GROUP_IMAGE(GROUP_SUBDIR), MSG_ATTACH(MSG_SUBDIR);

        private String subdir;

        Type(String photoSubdir) {
            subdir = photoSubdir;
        }

        protected String getSubdir() {
            return subdir;
        }
    }
}
