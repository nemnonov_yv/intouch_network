package ru.nemnonovyuri.intouch.model;

public enum MemberRole {
    ADMIN(1), USER(0), MODERATOR(2);

    private int numeric;

    MemberRole(int i) {
        numeric = i;
    }

    public static MemberRole valueOf(int numeric) {
        for (MemberRole role : MemberRole.values()) {
            if (role.getNumeric() == numeric) {
                return role;
            }
        }
        throw new RuntimeException("Invalid role numeric presentation");
    }

    public int getNumeric() {
        return numeric;
    }
}
