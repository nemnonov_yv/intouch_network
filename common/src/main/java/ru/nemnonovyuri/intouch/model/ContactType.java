package ru.nemnonovyuri.intouch.model;

public enum ContactType {
    PRIMARY_EMAIL(0),
    PERSONAL_PHONE(1),
    WORK_PHONE(2),
    HOME_ADDR(3),
    WORK_ADDR(4),
    EMAIL(5),
    ICQ(6),
    SKYPE(7);

    private int numeric;

    ContactType(int i) {
        numeric = i;
    }

    public static ContactType valueOf(int numeric) {
        for (ContactType type : ContactType.values()) {
            if (type.getNumeric() == numeric) {
                return type;
            }
        }
        throw new RuntimeException("Invalid contact numeric presentation");
    }

    public int getNumeric() {
        return numeric;
    }
}
