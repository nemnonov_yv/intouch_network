<form method="post" action="/profile/update/password" id="password-form">
    <input type="hidden" name="page" value="3">

    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="oldPassword" class="form-control" id="password" required>
    </div>
    <div class="form-group">
        <label for="new_password">New password</label>
        <input type="password" name="newPassword" class="form-control" id="new_password" required>
    </div>

    <button type="submit" class="btn btn-default">Save</button>
</form>