package ru.nemnonovyuri.intouch.model;

import ru.nemnonovyuri.intouch.model.converters.GroupMemberJpaConverter;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class GroupMembership implements Serializable {
    private static final long serialVersionUID = 777L;

    @ManyToOne
    private Account member;
    @ManyToOne
    private Group group;

    @Convert(converter = GroupMemberJpaConverter.class)
    private MemberRole role;

    public GroupMembership() {
    }

    public GroupMembership(Account member, Group group) {
        this(member, group, MemberRole.USER);
    }

    public GroupMembership(Account member, Group group, MemberRole role) {
        this.member = member;
        this.group = group;
        this.role = role;
    }

    public Account getMember() {
        return member;
    }

    public void setMember(Account member) {
        this.member = member;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public MemberRole getRole() {
        return role;
    }

    public void setRole(MemberRole role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupMembership that = (GroupMembership) o;

        if (!getMember().equals(that.getMember())) return false;
        return getGroup().equals(that.getGroup());

    }

    @Override
    public int hashCode() {
        int result = getMember().hashCode();
        result = 31 * result + getGroup().hashCode();
        return result;
    }
}
