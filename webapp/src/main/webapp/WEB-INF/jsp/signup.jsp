<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@page import="Sex" %>--%>
<%@page import="ru.nemnonovyuri.intouch.model.Sex" %>
<jsp:include page="header.jsp"/>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <h3>Register</h3>

        <p>Fill registration form</p>

        <form action="<c:url value="/signup"/>" method="post">
            <c:if test="${errors.size() > 0}">
                <div class="bg-danger">
                    <h4>Registration failed</h4>

                    <p>Fix following errors and try again</p>
                    <ul>
                        <c:forEach var="entry" items="${errors}">
                            <li><b>${entry.key}</b> - ${entry.value}</li>
                        </c:forEach>
                    </ul>
                </div>
            </c:if>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" id="name" value="${account.name}" required
                       autocomplete="off">
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="email" name="email" class="form-control" id="email" value="${email}" required
                       autocomplete="off">
            </div>
            <div class="form-group">
                <label for="login">Login</label>
                <input type="text" name="login" class="form-control" id="login" value="${account.login}" required
                       autocomplete="off">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password" required>
            </div>
            <div class="form-group">
                <label for="sex">Sex</label>
                <select name="sex" id="sex" class="form-control">
                    <option value="${Sex.UNSPECIFIED.getNumeric()}"
                            <c:if test="${account.sex == 'UNSPECIFIED'}">selected</c:if>>
                        ${Sex.UNSPECIFIED}
                    </option>
                    <option value="${Sex.MALE.getNumeric()}" <c:if test="${account.sex == 'MALE'}">selected</c:if>>
                        ${Sex.MALE}
                    </option>
                    <option value="${Sex.FEMALE.getNumeric()}" <c:if test="${account.sex == 'FEMALE'}">selected</c:if>>
                        ${Sex.FEMALE}
                    </option>
                </select>
            </div>
            <button type="submit" class="btn btn-default">Register</button>
        </form>
    </div>
</div>
<jsp:include page="footer.jsp"/>