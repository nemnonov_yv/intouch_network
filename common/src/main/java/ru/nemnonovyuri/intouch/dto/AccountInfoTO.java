package ru.nemnonovyuri.intouch.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import ru.nemnonovyuri.intouch.model.Account;
import ru.nemnonovyuri.intouch.model.Sex;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@JacksonXmlRootElement(localName = "user")
public class AccountInfoTO {
    private String login;
    private String name;
    private Date birthDate;
    private Sex sex;
    @JacksonXmlElementWrapper(localName = "contacts")
    private Set<ContactTO> contact;

    public AccountInfoTO() {
        contact = new HashSet<>();
    }

    public AccountInfoTO(Account account) {
        this();
        login = account.getLogin();
        name = account.getName();
        birthDate = account.getBirthDate();
        sex = account.getSex();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Set<ContactTO> getContact() {
        return contact;
    }

    public void setContact(Set<ContactTO> contact) {
        this.contact = contact;
    }
}
